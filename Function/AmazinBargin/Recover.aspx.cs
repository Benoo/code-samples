﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class Recover : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string key = Request.QueryString["key"];
            if(key == "" || key == null)
            {
                Response.Redirect("Home.aspx");
                return;
            }
            var FRecovery = new Function.PasswordRecovery();
            var recoveries = FRecovery.getAllPasswordRecoverys();
            var recovery = recoveries.Where(x => x.RecoveryKey == key && (x.RecoveryRequested - DateTime.Now).TotalHours <= 12).FirstOrDefault();
            if(recovery == null)
            {
                Response.Redirect("Home.aspx");
                return;
            }else
            {
                Recovery.Visible = true;
                RecoverPassword.loadRecovery(recovery);
                return;
            }
        }
    }
}