﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class MyAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Sedit = Request.QueryString["edit"];
            string Screate = Request.QueryString["create"];
            createAAccount.Visible = false;
            editAccount.Visible = false;
            login.Visible = false;
            accountsActivity.Visible = false;
            verify.Visible = false;
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }

            var loggedin = false;
            if(cokkie != null)
            {
                if(cokkie != null || cokkie != "")
                {
                    loggedin = true;
                }
            }

            if (Sedit == "edit" && loggedin)
            {
                //Edit Account
                editAccount.Visible = true;
                editAccountDetails.LoadAccountDetails(cokkie);
                return;
            }
            else
            {
                if(Screate == "create" && !loggedin)
                {
                    createAAccount.Visible = true;
                    return;
                }else
                {
                    if(Screate == "verify" && !loggedin)
                    {
                        verify.Visible = true;
                        return;
                    }
                    
                    if (loggedin)
                    {
                        accountsActivity.Visible = true;
                        accountActivity.loadMyDetails(cokkie);
                    }
                    else
                    {
                        login.Visible = true;
                    }
                }
            }
        }
    }
}