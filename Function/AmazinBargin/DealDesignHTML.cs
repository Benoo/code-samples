﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AmazinBargin
{
    public class DealDesignHTML
    {
        string page;
        public DealDesignHTML(string page)
        {
            this.page = page;
        }
        public string createDealHTML(Models.Deal deal, Models.Forum forum, int posts, Models.Catergory cat, Models.User user)
        {
            string html = "";

             html = $@"<div class=""dealTease"">
                        <div class=""dealTeaseLeft"">
                            {getImage(user)}
                            {getDealLikes(deal)}
                            {getDealDislikes()}
                        </div>
                        <div class=""dealTeaseCenter"">
                            <div class=""dealTeaseTitle"">
                                {getDealTitle(forum)}
                            </div>
                            <br /> 
                            by  
                            {getDealUsername(user)} 
                            <br />
                            {getDealText(forum)}
                            <br />
                            <br />
                            {getDealCommentAmount(posts)}
                            {getDealCatergory(cat)}
                            {forum.DateOfFounding.ToShortDateString()}
                        </div>
                        <div class=""dealTeaseRight"">
                            {getDealImage(deal)}
                            <br />
                        </div>
                    </div>
                ";
 

            return html;
        }

        public string getImage(Models.User user)
        {
            if(user == null)
            {
                return "";
            }
            string img = "";
            switch (user.UserTypeID)
            {
                case 0:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
                case 1:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
                case 2:
                    img = "<img src=\"../Images/moderator.png\" alt=\"\">";
                    break;
                case 3:
                    img = "<img src=\"../Images/bronzeuser.png\" alt=\"\">";
                    break;
                case 4:
                    img = "<img src=\"../Images/silveruser.png\" alt=\"\">";
                    break;
                case 5:
                    img = "<img src=\"../Images/golduser.png\" alt=\"\">";
                    break;
                default:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
            }

            return img;
        }
        public string getDealLikes(Models.Deal deal)
        {
            if(deal == null)
            {
                return "";
            }
            var FUserDeals = new Function.UserDealLike();
            var UserDeals = FUserDeals.getAllUserDealLikes();
            int votes = UserDeals.Where(x => x.DealID == deal.DealID && x.LikedDeal == true).Count();
            return "<em class=\"small\">" + "Votes: " + votes + " </em>";
        }
        public string getDealDislikes()
        {
            return "";
        }
        public string getDealTitle(Models.Forum forum)
        {

            var name = forum.ForumName.Split(' ');
            string fname = "";
            foreach(var n in name)
            {         
                if (n.Contains('$'))
                {
                    string before = "";
                    string after = "";
                    string required = "$1234567890,.";
                    for(var i = 0; i < n.Count(); i++)
                    {
                        var c = n[i];
                        if (required.Contains(c))
                        {
                            before += c;
                        }else
                        {
                            after += c;
                        }
                    }
                    fname += "<em class=\"money\">" + before + "</em>" + after + " ";
                }else
                {
                    fname += n + " ";
                }
            }

            string title = "<a href = \"../"+page+".aspx?node="+ forum.ForumID + "\"> " + fname + " </a>";
            return title;
        }
        public string getDealUsername(Models.User user)
        {
            if (user != null)
            {
                return "<em class=\"small\">" + user.Username + " </em>";
            }
            return "";
        }
        public string getDealText(Models.Forum forum)
        {
            return forum.Description.Substring(0, Math.Min(forum.Description.Length, 200)); ;
        }
        public string getDealCommentAmount(int posts)
        {         
             return "<img src=\"../Images/Comments.png\" alt=\"\"><em class=\"small\">" + posts + " </em>";      
        }
        public string getDealCatergory(Models.Catergory cat)
        {
            if (cat != null)
            {
               return "<em class=\"small\">" + cat.CatergoryName + " </em>";
            }

            return "";
        }
        public string getDealImage(Models.Deal deal)
        {
            if(deal == null)
            {
                return "";
            }
            return "<img src=\""+ deal.DealImage +"\" alt=\"\" width=\"110\" height=\"110\">";
        }
    }
}