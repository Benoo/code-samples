﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AmazinBargin
{
    public class DealDesignHTMLSideBar
    {
        string page;
        public DealDesignHTMLSideBar(string page)
        {
            this.page = page;
        }
        public string createDealHTML(Models.Deal deal, Models.Forum forum, int posts, Models.Catergory cat, Models.User user)
        {
            string html = "";

            html = $@"<div class=""sidebarTease"">
                        <div class=""sidebarLeft"">
                            {getImage()}
                            {getDealLikes()}
                            {getDealDislikes()}
                        </div>
                        <div class=""sidebarCenter"">
                            <div class=""sidebarTeaseTitle"">
                                {getDealTitle(forum)}
                            </div>
                            {getDealCommentAmount(posts)}
                            {getDealCatergory(cat)}
                            {forum.DateOfFounding.ToShortDateString()}
                        </div>
                        <div class=""sidebarRight"">
                            {getDealImage()}
                            <br />
                        </div>
                    </div>
                    ";

            return html;
        }

        public string getImage()
        {
            return "";
        }
        public string getDealLikes()
        {
            return "";
        }
        public string getDealDislikes()
        {
            return "";
        }
        public string getDealTitle(Models.Forum forum)
        {

            var name = forum.ForumName.Split(' ');
            string fname = "";
            foreach(var n in name)
            {         
                if (n.Contains('$'))
                {
                    string before = "";
                    string after = "";
                    string required = "$1234567890,.";
                    for (var i = 0; i < n.Count(); i++)
                    {
                        var c = n[i];
                        if (required.Contains(c))
                        {
                            before += c;
                        }
                        else
                        {
                            after += c;
                        }
                    }
                    fname += "<em class=\"money\">" + before + "</em>" + after + " ";
                }
                else
                {
                    fname += n + " ";
                }
            }

            string title = "<a href = \"../"+page+".aspx?node="+ forum.ForumID + "\"> " + fname + " </a>";
            return title;
        }
        public string getDealUsername(Models.User user)
        {
            if (user != null)
            {
                return "<em class=\"small\">" + user.Username + " </em>";
            }
            return "";
        }
        public string getDealText(Models.Forum forum)
        {
            return forum.Description.Substring(0, Math.Min(forum.Description.Length, 200)); ;
        }
        public string getDealCommentAmount(int posts)
        {         
             return "<img src=\"../Images/Comments.png\" alt=\"\"><em class=\"small\">" + posts + " </em>";      
        }
        public string getDealCatergory(Models.Catergory cat)
        {
            if (cat != null)
            {
               return "<em class=\"small\">" + cat.CatergoryName + " </em>";
            }

            return "";
        }
        public string getDealImage()
        {
            return "";
        }
    }
}