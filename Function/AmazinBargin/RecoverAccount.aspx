﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecoverAccount.aspx.cs" Inherits="AmazinBargin.RecoverAccount" %>
<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>


<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
       <uc1:Menu runat="server" ID="Menu" />
         <br />
        <div class="maincontent">
            <div class="dealTeaseTitle">
                Recover Account
            </div>
            You can use this service to get an email sent to the email address you signed up with. That email will contain a link
            that you can click to recover your account<br />
            <br />
            <br />
            Please enter your email below and click 'Recover' to begin the process.<br /><br />
            <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
            <asp:Button ID="Button1" runat="server" Text="Recover" OnClick="Button1_Click" CssClass="normalButton" />
            <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
        </div>
         <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
