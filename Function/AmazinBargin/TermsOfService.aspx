﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TermsOfService.aspx.cs" Inherits="AmazinBargin.TermsOfService" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>


<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Terms Of Service</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Menu runat="server" ID="Menu" />
         <br />
        <div class="maincontent">
            <div class="dealTeaseTitle">
                 Terms of Service
            </div><br />
            This website is made avaliable to you for personal use only. You must provide correct and
            current information if you wish to create an account and use the service. You are responsible
            for maintaining the accuracy of the infomation you submit. We reserve the right to refuse 
            service to anyone at any time without notice or reason.<br /><br />
            <div class="dealTeaseTitle">
                 Proper Use
            </div><br />
            You agree that you are responsible for any of the information or communications you supply to us and except any
            consequences from the supply of those. Your use of the Service is subject to your acceptance of
            and compliance with the Agreements. You agree to use the service in compliance with all 
            applicable laws, rules and regulations from which your subjected to.<br />
            <br />
            We reserve the right but shall have no obligation, to investigate your use of the Service in
            order to determine if a violation of the Agreement has occurred.<br />
            <br />
            Multiple accounts are subject to immediate termination unless authorised by us. 

        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
