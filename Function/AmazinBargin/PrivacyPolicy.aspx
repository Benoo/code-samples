﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrivacyPolicy.aspx.cs" Inherits="AmazinBargin.PrivacyPolicy" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>


<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Privacy Policy</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Menu runat="server" ID="Menu" />
        <br />
        <div class="maincontent">
            <div class="dealTeaseTitle">
                 Privacy Policy
            </div>
            <br />
                This Privacy Policy is subject to change due the changing environment of Internet technologies.
                This site is not responsible for the content or privacy policy of sites it links to.
            <br /><br />
            <div class="dealTeaseTitle">
                 Information We Collect
            </div>
            <br />   
            We collect infomation about you so that we can provide the best service we can. <br /><br />
            For Account Holders:<br /><br />
            Username<br />
            Email<br />
            IP address<br />
            <br />
            Optional:<br /><br />
            Your Website URL<br />
            Your Gender<br />
            Avatar<br />
            <br /><br />
            <div class="dealTeaseTitle">
                 Who else access to the information
            </div>
            <br />
            The public can see:<br /><br />
            Username:<br />
            Website URL<br />
            Avatar<br />
            <br /><br />
            <div class="dealTeaseTitle">
                 Cookies
            </div>
            <br />
            We or 3rd parties may place cookies on your web browser. Cookies are used to help identify you
            when you visit this website and other websites.
            <br />

        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
