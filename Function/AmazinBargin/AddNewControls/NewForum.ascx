﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewForum.ascx.cs" Inherits="AmazinBargin.AddNewControls.NewForum" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="forum" class="forum" runat="server">
    <asp:Label ID="DealsTitle" runat="server" Text="Create New Forum" CssClass="dealTeaseTitle"></asp:Label><br />
    <br />
    <div id="forums" runat="server">
        Subject*<br />
        <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
        Forum Category*<br />
        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList><br /><br />
        Body*<br />
        <asp:TextBox ID="TextBox2" runat="server" CssClass="textboxcontrol" Height="305px" TextMode="MultiLine"></asp:TextBox><br /><br />
    </div>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Submit Forum Topic"  CssClass="normalButton" OnClick="Button1_Click"/>
</div>