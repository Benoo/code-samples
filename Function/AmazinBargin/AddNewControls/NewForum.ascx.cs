﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.AddNewControls
{
    public partial class NewForum : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void loadCatergories()
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Clear();
                var FCat = new Function.Catergory();
                var cats = FCat.getAllCatergorys().Where(x => x.DealOrForum == true).ToList();
                foreach (var cat in cats)
                {
                    DropDownList1.Items.Add(new ListItem(cat.CatergoryName, cat.CatergoryID.ToString()));
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Confirm User
            TextBox1.BackColor = System.Drawing.Color.White;
            TextBox2.BackColor = System.Drawing.Color.White;
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if(cokkie == null) {
                return;
            }
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
            if(user == null)
            {
                return;
            }
            //Check If user allowed to post Forum
            var Fforumsa = new Function.Forum();
            var foruma = Fforumsa.getAllForums().Where(x => x.FoundingUserID == user.UserID && x.DateOfFounding > DateTime.Now.AddDays(-1)).ToList();
            if (user.UserTypeID == 1 || user.UserTypeID == 3)
            {
                var postedInLast2Hours = foruma.Where(x => (x.DateOfFounding - DateTime.Now).TotalHours < 2 && (x.DateOfFounding - DateTime.Now).TotalHours > -2).FirstOrDefault();
                if (postedInLast2Hours != null)
                {
                    Response.Write("<script>alert('You are only allowed to post 1 deal or 1 forum every 2 hours. Please wait until 2 hours have elapsed between posting.');</script>");
                    return;
                }
            }
            if (user.UserTypeID == 4)
            {
                var postedInLast2Hours = foruma.Where(x => (x.DateOfFounding - DateTime.Now).TotalHours < 1 && (x.DateOfFounding - DateTime.Now).TotalHours > -1).FirstOrDefault();
                if (postedInLast2Hours != null)
                {
                    Response.Write("<script>alert('You are only allowed to post 1 deal or 1 forum every 1 hour. Please wait until 1 hour has elapsed between posting.');</script>");
                    return;
                }
            }

            //Validate Input
            if (TextBox1.Text == null || TextBox1.Text == "")
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                return;
            }
            if (TextBox1.Text.Count() < 20)
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                Response.Write("<script>alert('Subject needs to be at least 20 characters');</script>");
                return;
            }         
            if (TextBox2.Text.Count() < 100)
            {
                //TextBox2.BackColor = System.Drawing.Color.Red;
                Response.Write("<script>alert('Body needs to be at least 100 characters');</script>");
                return;
            }
            if (DropDownList1.SelectedIndex < 0)
            {
                return;
            }

            if (TextBox2.Text.Contains("<") || TextBox2.Text.Contains(">"))
            {
                TextBox2.BackColor = System.Drawing.Color.Red;
                return;
            }
            if (TextBox1.Text.Contains("<") || TextBox1.Text.Contains(">"))
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                return;
            }

            //Create New Forum
            var forum = new Models.Forum(
                0,
                TextBox1.Text,
                int.Parse(DropDownList1.SelectedItem.Value),
                user.UserID,
                TextBox2.Text,
                DateTime.Now,
                null,
                null,
                0,
                null
                );

            var Fforums = new Function.Forum();
            Fforums.saveForum(forum);
            Response.Redirect("MyAccount.aspx");
        }
    }
}