﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.AddNewControls
{
    public partial class NewDeal : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void loadCatergories()
        {
            if (!IsPostBack)
            {
                DropDownList1.Items.Clear();
                var FCat = new Function.Catergory();
                var cats = FCat.getAllCatergorys().Where(x => x.DealOrForum == false).ToList();
                foreach (var cat in cats)
                {
                    DropDownList1.Items.Add(new ListItem(cat.CatergoryName, cat.CatergoryID.ToString()));
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Confirm User
            TextBox1.BackColor = System.Drawing.Color.White;
            TextBox2.BackColor = System.Drawing.Color.White;
            TextBox4.BackColor = System.Drawing.Color.White;
            TextBox5.BackColor = System.Drawing.Color.White;
            TextBox6.BackColor = System.Drawing.Color.White;
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if (cokkie == null)
            {
                return;
            }
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
            if (user == null)
            {
                return;
            }

            //Check If user allowed to post Deal
            var Fforumsa = new Function.Forum();
            var foruma = Fforumsa.getAllForums().Where(x => x.FoundingUserID == user.UserID && x.DateOfFounding > DateTime.Now.AddDays(-1)).ToList();
            if (user.UserTypeID == 1 || user.UserTypeID == 3)
            {
                var postedInLast2Hours = foruma.Where(x => (x.DateOfFounding - DateTime.Now).TotalHours < 2 && (x.DateOfFounding - DateTime.Now).TotalHours > -2).FirstOrDefault();
                if (postedInLast2Hours != null)
                {
                    Response.Write("<script>alert('You are only allowed to post 1 deal or 1 forum every 2 hours. Please wait until 2 hours have elapsed between posting.');</script>");
                    return;
                }
            }
            if (user.UserTypeID == 4)
            {
                var postedInLast2Hours = foruma.Where(x => (x.DateOfFounding - DateTime.Now).TotalHours < 1 && (x.DateOfFounding - DateTime.Now).TotalHours > -1).FirstOrDefault();
                if (postedInLast2Hours != null)
                {
                    Response.Write("<script>alert('You are only allowed to post 1 deal or 1 forum every 1 hour. Please wait until 1 hour has elapsed between posting.');</script>");
                    return;
                }
            }

            //Validate Input
            if (TextBox1.Text == null || TextBox1.Text == "")
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                return;
            }
            if(TextBox1.Text.Count() < 20)
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                Response.Write("<script>alert('Subject needs to be at least 20 characters');</script>");
                return;
            }
            if (TextBox2.Text.Count() < 100)
            {
                //TextBox2.BackColor = System.Drawing.Color.Red;
                Response.Write("<script>alert('Body needs to be at least 100 characters');</script>");
                return;
            }
            if (DropDownList1.SelectedIndex < 0)
            {
                return;
            }
            //Validate Deal Input
            DateTime startDate;
            DateTime endDate;
            decimal savings;
            try
            {
                startDate = DateTime.Parse(TextBox4.Text);
            }
            catch
            {
                TextBox4.BackColor = System.Drawing.Color.Red;
                return;
            }
            try
            {
                endDate = DateTime.Parse(TextBox5.Text);
            }
            catch
            {
                TextBox5.BackColor = System.Drawing.Color.Red;
                return;
            }
            try
            {
                savings = decimal.Parse(TextBox6.Text);
            }
            catch
            {
                savings = 0;
            }

            if(TextBox3.Text.Contains("<")|| TextBox3.Text.Contains(">"))
            {
                TextBox3.BackColor = System.Drawing.Color.Red;
                return;
            }
            if (TextBox2.Text.Contains("<") || TextBox2.Text.Contains(">"))
            {
                TextBox2.BackColor = System.Drawing.Color.Red;
                return;
            }
            if (TextBox1.Text.Contains("<") || TextBox1.Text.Contains(">"))
            {
                TextBox1.BackColor = System.Drawing.Color.Red;
                return;
            }

            //Create New Deal
            var deal = new Models.Deal(
                0,
                savings,
                0,
                TextBox3.Text,
                0,
                startDate,
                endDate, uploadF()
                );

            var Fdeals = new Function.Deal();
            Fdeals.saveDeal(deal);
            var web = createWebsite(TextBox7.Text);
            //Create New Forum
            var forum = new Models.Forum(
                0,
                TextBox1.Text,
                int.Parse(DropDownList1.SelectedItem.Value),
                user.UserID,
                TextBox2.Text,
                DateTime.Now,
                web.WebsiteID,
                web.WebsiteLink,
                0,
                deal.DealID
                );

            var Fforums = new Function.Forum();
            Fforums.saveForum(forum);
            Response.Redirect("MyAccount.aspx");
        }
        public string uploadF()
        {
            if (IsPostBack)
            {
                Boolean fileOK = false;
                String path = Server.MapPath("~/Images/");
                string randomFileName = getRandomString();
                if (FileUpload1.HasFile)
                {
                    String fileExtension =
                        System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    String[] allowedExtensions =
                        {".gif", ".png", ".jpeg", ".jpg"};
                    for (int i = 0; i < allowedExtensions.Length; i++)
                    {
                        if (fileExtension == allowedExtensions[i])
                        {
                            fileOK = true;
                        }
                    }
                }

                if (fileOK)
                {
                    try
                    {
                        FileUpload1.PostedFile.SaveAs(path + "o" + randomFileName + System.IO.Path.GetExtension(FileUpload1.FileName).ToLower());
                        Bitmap bitmap = new Bitmap(path + "o" + randomFileName + System.IO.Path.GetExtension(FileUpload1.FileName).ToLower());

                        float width = 350;
                        float height = 350;
                        var brush = new SolidBrush(Color.Transparent);
                        float scale = Math.Min(width / bitmap.Width, height / bitmap.Height);
                        var bmp = new Bitmap((int)width, (int)height);
                        var graph = Graphics.FromImage(bmp);

                        // uncomment for higher quality output
                        //graph.InterpolationMode = InterpolationMode.High;
                        //graph.CompositingQuality = CompositingQuality.HighQuality;
                        //graph.SmoothingMode = SmoothingMode.AntiAlias;

                        var scaleWidth = (int)(bitmap.Width * scale);
                        var scaleHeight = (int)(bitmap.Height * scale);
                        graph.FillRectangle(brush, new RectangleF(0, 0, width, height));
                        graph.DrawImage(bitmap, ((int)width - scaleWidth) / 2, ((int)height - scaleHeight) / 2, scaleWidth, scaleHeight);
                        bmp.Save(path + randomFileName + System.IO.Path.GetExtension(FileUpload1.FileName).ToLower());

                        return "Images/" + randomFileName + System.IO.Path.GetExtension(FileUpload1.FileName).ToLower();
                    }
                    catch (Exception ex)
                    {
                       
                    }
                }
                else
                {
                 
                }
            }
            return "";
        }
        public string getRandomString(int n = 10)
        {
            string ran = "";
            var random = new Random((int)DateTime.Now.Ticks + DateTime.Now.Second);
            string pic = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789875285328925723109341";
            for(var i = 0; i < n; i++)
            {
                ran += pic.Substring(random.Next(0, 70), 1);
            }

            return ran;
        }

        private Models.Website createWebsite(string link)
        {
            string links = "SL" + getRandomString(15);
            var web = new Models.Website(0, link, links);
            var FWeb = new Function.Website();
            FWeb.saveWebsite(web);
            return web;
        }
    }
}