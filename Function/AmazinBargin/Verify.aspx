﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Verify.aspx.cs" Inherits="AmazinBargin.Verify" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <uc1:Menu runat="server" ID="Menu" />
         <br />
        <div class="maincontent">
             <div class="dealTeaseTitle">
                 Thank you for verifying your account
            </div><br />
            <br />
            You are now able to log in and enjoy all the benifits.
        </div>
         <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
