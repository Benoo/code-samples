﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class AddNew : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Sadd = Request.QueryString["add"];
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if(cokkie == null)
            {
                Response.Redirect("Home.aspx");
            }
            deal.Visible = false;
            forum.Visible = false;
            switch (Sadd)
            {
                case"deal":
                    deal.Visible = true;
                    NewDeal.loadCatergories();
                    break;

                case "forum":
                    forum.Visible = true;
                    NewForum.loadCatergories();
                    break;
                default:
                    Response.Redirect("Home.aspx");
                    break;
            }


        }
    }
}