﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class Deals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string Spage = Request.QueryString["node"];
            string Scat = Request.QueryString["category"];
            if (Spage == null || Spage == "")
            {
                ForumSelection.loadCatergories(Scat,"Deals");
                forumselectiondiv.Visible = true;
                forumdiv.Visible = false;
            }
            else
            {
                int load = -1;
                try
                {
                    load = int.Parse(Spage);
                }
                catch { }
                if (!IsPostBack)
                {
                    Forum.LoadForum(load);
                }
                forumselectiondiv.Visible = false;
                forumdiv.Visible = true;

            }
        }
    }
}