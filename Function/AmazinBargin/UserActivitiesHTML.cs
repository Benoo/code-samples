﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazinBargin
{
    public class UserActivitiesHTML
    {
        string page = "Forums";
        public string createDealHTML(Models.Deal deal, Models.Forum forum, int posts, Models.Catergory cat, Models.User user)
        {
            if(deal == null)
            {
                page = "Forums";
            }
            else
            {
                page = "Deals";
            }
            string html = "";
            string firstPart = "<div class=\"dealTease\">" +
                               "<div class=\"dealTeaseLeft\">";
            string userImage = getImage();
            string likes = getDealLikes();
            string dislikes = getDealDislikes();
            string secondPart = "</div>" +
                                 "<div class=\"dealTeaseCenter\">" +
                                 "<div class=\"dealTeaseTitle\">";
            string title = getDealTitle(forum);
            string thirdpart = "</div><br /> by ";
            string username = getDealUsername(user);
            string fourth = "<br />";
            string dealText200char = getDealText(forum);
            string fith = "<br /><br />";
            string commentAmount = getDealCommentAmount(posts);
            string catergory = getDealCatergory(cat);
            string timeofDeal = forum.DateOfFounding.ToShortDateString();
            string sixth = "</div>" +
                           "<div class=\"dealTeaseRight\">";
            string imageOfDeal = getDealImage();
            string seventh = "<br />" +
                             "<br />" +
                             "</div>" +
                             "</div>";


            html = firstPart +
                userImage +
                likes +
                dislikes +
                secondPart +
                title +
                thirdpart +
                username +
                fourth +
                dealText200char +
                fith +
                commentAmount +
                catergory +
                timeofDeal +
                sixth +
                imageOfDeal +
                seventh;

            return html;
        }

        public string getImage()
        {
            return "";
        }
        public string getDealLikes()
        {
            return "";
        }
        public string getDealDislikes()
        {
            return "";
        }
        public string getDealTitle(Models.Forum forum)
        {

            var name = forum.ForumName.Split(' ');
            string fname = "";
            foreach (var n in name)
            {
                if (n.Contains('$'))
                {
                    string before = "";
                    string after = "";
                    string required = "$1234567890,.";
                    for (var i = 0; i < n.Count(); i++)
                    {
                        var c = n[i];
                        if (required.Contains(c))
                        {
                            before += c;
                        }
                        else
                        {
                            after += c;
                        }
                    }
                    fname += "<em class=\"money\">" + before + "</em>" + after + " ";
                }
                else
                {
                    fname += n + " ";
                }
            }

            string title = "<a href = \"../" + page + ".aspx?node=" + forum.ForumID + "\"> " + fname + " </a>";
            return title;
        }
        public string getDealUsername(Models.User user)
        {
            if (user != null)
            {
                return "<em class=\"small\">" + user.Username + " </em>";
            }
            return "";
        }
        public string getDealText(Models.Forum forum)
        {
            return forum.Description.Substring(0, Math.Min(forum.Description.Length, 200)); ;
        }
        public string getDealCommentAmount(int posts)
        {
            return "<img src=\"../Images/Comments.png\" alt=\"\"><em class=\"small\">" + posts + " </em>";
        }
        public string getDealCatergory(Models.Catergory cat)
        {
            if (cat != null)
            {
                return "<em class=\"small\">" + cat.CatergoryName + " </em>";
            }

            return "";
        }
        public string getDealImage()
        {
            return "";
        }
    }
}