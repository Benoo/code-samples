﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecoverPassword.ascx.cs" Inherits="AmazinBargin.userControls.emailControls.RecoverPassword" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<div id ="recover" runat="server" visible="false" style="width:50%">
    New Password<br /> <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol" TextMode="Password"></asp:TextBox>
    <br /><br />
    <asp:Button ID="Button1" runat="server" Text="Change Password"  CssClass="normalButton" OnClick="Button1_Click"/>
</div>