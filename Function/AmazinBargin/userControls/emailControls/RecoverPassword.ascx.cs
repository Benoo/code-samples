﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls.emailControls
{
    public partial class RecoverPassword : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void loadRecovery(Models.PasswordRecovery recovery)
        {
            recover.Visible = true;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string key = Request.QueryString["key"];
            if (key == "" || key == null)
            {
                Response.Redirect("Home.aspx");
                return;
            }
            var FRecovery = new Function.PasswordRecovery();
            var recoveries = FRecovery.getAllPasswordRecoverys();
            var recovery = recoveries.Where(x => x.RecoveryKey == key && (x.RecoveryRequested - DateTime.Now).TotalHours <= 12).FirstOrDefault();
            if (recovery == null)
            {
                Response.Redirect("Home.aspx");
                return;
            }

            var FUser = new Function.User();
            var user = FUser.getUsers(recovery.UserID);
            if(user == null)
            {
                Response.Redirect("Home.aspx");
                return;
            }
            user.Password = TextBox1.Text;
            FUser.saveUser(user);
            FRecovery.deletePasswordRecovery(recovery);
            Response.Redirect("Home.aspx");
            return;
        }
    }
}