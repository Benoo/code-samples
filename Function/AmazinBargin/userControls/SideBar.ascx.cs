﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class SideBar : System.Web.UI.UserControl
    {
        int maxShown = 10;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var fForums = new Function.Forum();
                var forums = fForums.getAllForums();
                loadLatestDeals(forums);
                loadLatestForums(forums);
            }
        }

        private void loadLatestDeals(List<Models.Forum> forums)
        {
            var FDeal = new Function.Deal();
            var deals = FDeal.getAllDeals().OrderByDescending(x => x.DealID).Take(maxShown).ToList();
            NewDeals.InnerHtml = "";
            var createHTML = new DealDesignHTMLSideBar("Deals");
            var html = "";
            var Fcat = new Function.Catergory();
            var cat = Fcat.getAllCatergorys();
            var FUser = new Function.User();
            var Fposts = new Function.ForumPost();
        
            foreach(var deal in deals)
            {
                var forum = forums.Where(x => x.DealID == deal.DealID).FirstOrDefault();
                var user = FUser.getUsers(forum.FoundingUserID);
                var posts = Fposts.getAllForumPosts().Where(x => x.ForumID == forum.ForumID).Count();
                html += createHTML.createDealHTML(deal, forum, posts, cat.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault(), user);
            }
            NewDeals.InnerHtml = html;
        }

        private void loadLatestForums(List<Models.Forum> forums)
        {
            NewForums.InnerHtml = "";
            forums = forums.Where(y=>y.DealID == null).OrderByDescending(x => x.ForumID).Take(maxShown).ToList();
            var createHTML = new DealDesignHTMLSideBar("Forums");
            var html = "";
            var Fcat = new Function.Catergory();
            var cat = Fcat.getAllCatergorys();
            var FUser = new Function.User();
            var Fposts = new Function.ForumPost();
            foreach(var forum in forums)
            {        
                var user = FUser.getUsers(forum.FoundingUserID);
                var posts = Fposts.getAllForumPosts().Where(x => x.ForumID == forum.ForumID).Count();
                html += createHTML.createDealHTML(null, forum, posts, cat.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault(), user);
            }
            NewForums.InnerHtml = html;
        }
    }
}