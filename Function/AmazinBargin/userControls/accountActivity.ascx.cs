﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class accountActivity : System.Web.UI.UserControl
    {
        int limitResults = 10;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("MyAccount.aspx?edit=edit");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var user = new ValidateUser();
            user.logout(Response);
            Response.Redirect("Home.aspx");
        }

        public void loadMyDetails(string username)
        {
            Label1.Text = "";
            Label2.Text = "";
            Label3.Text = "";
          
                var fUser = new Function.User();
                var user = fUser.getAllUsers().Where(x => x.Username == username).FirstOrDefault();
                if (user == null)
                {
                    return;
                }
            if (!IsPostBack)
            {
                if (user.UserTypeID == 2)
                {
                    moderatorSection.Visible = true;
                    ModUserID.Value = user.UserID.ToString();
                    var users = fUser.getAllUsers().Where(x => x.UserTypeID != 2).OrderBy(y=>y.Username);          
                    DropDownList1.Items.Clear();
                    foreach (var auser in users)
                    {
                        DropDownList1.Items.Add(new ListItem(auser.Username + " " + auser.Email, auser.UserID.ToString()));
                    }
                    DropDownList2.Items.Clear();
                    var fUserType = new Function.UserType();
                    var types = fUserType.getAllUserTypes();
                    types = types.Where(x => x.UserTypeID != 2).ToList();
                    foreach (var auser in users)
                    {
                        var type = types.Where(x => x.UserTypeID == auser.UserTypeID).FirstOrDefault();
                        string stype = "Not Found";
                        if (type != null)
                        {
                            stype = type.UserType1;
                        }
                        DropDownList2.Items.Add(new ListItem(auser.Username + " " + stype, auser.UserID.ToString()));
                    }
                    DropDownList3.Items.Clear();
                    foreach(var type in types)
                    {
                        DropDownList3.Items.Add(new ListItem(type.UserType1, type.UserTypeID.ToString()));
                    }
                }
            }
            Label1.Text = user.Username;
            Label2.Text = user.JoinDate.ToShortDateString();
            Label3.Text = user.Location;
            loadActivities(user);
        }

        public void loadActivities(Models.User user)
        {
            string template = "";

            var activities = new UserActivitiesHTML();

            var function = new Function.Forum();
            var Fposts = new Function.ForumPost();
            var FCat = new Function.Catergory();
            var cats = FCat.getAllCatergorys();
            var posts = Fposts.getAllForumPosts();
            var FDeal = new Function.Deal();
            var deals = FDeal.getAllDeals();

            var forums = function.getAllForums().Where(x=>x.FoundingUserID == user.UserID).Reverse().Take(limitResults).ToList();

            foreach(var forum in forums)
            {
                var deal = deals.Where(x => x.DealID == forum.DealID).FirstOrDefault();
                var postcount = posts.Count(x => x.ForumID == forum.ForumID);
                var Gcat = cats.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault();
                if (Gcat == null)
                {
                    continue;
                }

                template += "";
                template += activities.createDealHTML(deal, forum, postcount, Gcat, user);
                template += "<br />";

            }

            loadUsersActivities.InnerHtml = template;
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            //Ban the selected User after checks
            var fUser = new Function.User();
            var id = -1;
            try
            {
                id = int.Parse(ModUserID.Value);
            }
            catch
            {
                return;
            }
            var user = fUser.getUsers(id);
            if(user == null)
            {
                return;
            }
            if(user.UserTypeID != 2)
            {
                return;
            }
            if(DropDownList1.SelectedIndex < 0)
            {
                return;
            }
            DateTime banto;
            try
            {
                banto = DateTime.Parse(TextBox1.Text);
            }
            catch
            {
                Response.Write("<script>alert('Ban Date Wrong');</script>");
                return;
            }
            //Get User
            var banUser = fUser.getUsers(int.Parse(DropDownList1.SelectedItem.Value));
            if(banUser == null)
            {
                Response.Write("<script>alert('User Not Found');</script>");
                return;
            }
            banUser.BannedUntil = banto;
            fUser.saveUser(banUser);
            Response.Write("<script>alert('User Was Banned');</script>");

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            var fUser = new Function.User();
            var id = -1;
            try
            {
                id = int.Parse(ModUserID.Value);
            }
            catch
            {
                return;
            }
            var user = fUser.getUsers(id);
            if (user == null)
            {
                return;
            }
            if (user.UserTypeID != 2)
            {
                return;
            }
            if (DropDownList2.SelectedIndex < 0)
            {
                return;
            }
            if (DropDownList3.SelectedIndex < 0)
            {
                return;
            }
          
            //Get User
            var auser = fUser.getUsers(int.Parse(DropDownList1.SelectedItem.Value));
            if (auser == null)
            {
                Response.Write("<script>alert('User Not Found');</script>");
                return;
            }
            try
            {
                var fTypes = new Function.UserType();
                var type = fTypes.getUserType(int.Parse(DropDownList3.SelectedItem.Value));
            }
            catch
            {
                Response.Write("<script>alert('Type Not Found');</script>");
                return;
            }
            auser.UserTypeID = int.Parse(DropDownList3.SelectedItem.Value);
            fUser.saveUser(auser);
            Response.Write("<script>alert('User Type was changed');</script>");
        }
    }
}