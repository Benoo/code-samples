﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class LogIn : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("MyAccount.aspx?create=create");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            var user = new ValidateUser();
            if(user.login(TextBox1.Text, TextBox3.Text,Response, Request.UserHostAddress))
            {            
                Response.Redirect("MyAccount.aspx");
            }      
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
            Response.Redirect("RecoverAccount.aspx");
        }
    }
}