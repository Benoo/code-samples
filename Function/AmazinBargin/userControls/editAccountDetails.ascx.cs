﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class editAccountDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TextBox3.BackColor = System.Drawing.Color.White;
            TextBox4.BackColor = System.Drawing.Color.White;
            var cokkie = Request.Cookies["UserSettings"]["User"];
            if(cokkie == null)
            {
                return;
            }
            var fUser = new Function.User();
            var user = fUser.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
            if (user == null)
            {
                return;
            }
            if (TextBox3.Text != null && TextBox3.Text != "")
            {
                if (TextBox3.Text != user.Password)
                {
                    TextBox3.BackColor = System.Drawing.Color.Red;
                    return;
                }
                if (TextBox4.Text.Count() > 7 && !TextBox4.Text.Contains('<') && !TextBox4.Text.Contains('>'))
                {
                    user.Password = TextBox4.Text;
                    user.Location = TextBox5.Text;
                    fUser.saveUser(user);
                    Response.Redirect("MyAccount.aspx");
                }
                else
                {
                    TextBox4.BackColor = System.Drawing.Color.Red;
                    return;
                }
            }else
            {
                if (user.Location != TextBox5.Text)
                {
                    user.Location = TextBox5.Text;
                    fUser.saveUser(user);
                    Response.Redirect("MyAccount.aspx");
                }
            }
        }
        public void LoadAccountDetails(string username)
        {
            if (IsPostBack) {
                return;
            } 
            TextBox1.Text = "";
            TextBox2.Text = "";
            TextBox3.Text = "";
            TextBox4.Text = "";
            TextBox5.Text = "";

            var fUser = new Function.User();
            var user = fUser.getAllUsers().Where(x => x.Username == username).FirstOrDefault();
            if(user == null)
            {
                return;
            }

            TextBox1.Text = user.Username;
            TextBox2.Text = user.Email;
            TextBox5.Text = user.Location;


        }
    }
}