﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class ReplyToForum : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if(cokkie == null)
            {
                reply.Visible = false;
                notloggedin.Visible = true;
            }
            else
            {
                reply.Visible = true;
                notloggedin.Visible = false;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            //Get the node ID this is the forum we are replying to
            if (!checkPostContents()){
                Response.Write("<script>alert('Your comment contains banned words. Please remove them and think carefully before posting. There is no tolerance for bad behaviour');</script>");
                return;
            }
            if(TextBox1.Text.Count() < 20)
            {
                Response.Write("<script>alert('Your reply is too short. Please use at least 20 characters.');</script>");
                return;
            }
            string node = Request.QueryString["node"];
            var Fforums = new Function.Forum();
            var forum = Fforums.getAllForums().Where(x => x.ForumID.ToString() == node).FirstOrDefault();
            if(forum == null)
            {
                return;
            }
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
            if (user == null)
            {
                return;
            }
            var post = new Models.ForumPost(0
                , forum.ForumID
                , user.UserID
                , DateTime.Now
                , TextBox1.Text
                , null
                );

            var FPosts = new Function.ForumPost();
            FPosts.saveForumPost(post);
            Response.Redirect(Request.RawUrl);
        }
        private bool checkPostContents()
        {    
            if (TextBox1.Text.Contains("<") || TextBox1.Text.Contains(">"))
            {
                return false;
            }
            if (TextBox1.Text.ToLower().Contains("fuck") || TextBox1.Text.ToLower().Contains("cunt"))
            {
                return false;
            }
            if (TextBox1.Text.ToLower().Contains("bitch") || TextBox1.Text.ToLower().Contains("cock"))
            {
                return false;
            }
            if (TextBox1.Text.ToLower().Contains("slut") || TextBox1.Text.ToLower().Contains("ozbargin"))
            {
                return false;
            }


            return true;
        }
    }
}