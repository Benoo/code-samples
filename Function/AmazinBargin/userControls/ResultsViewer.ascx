﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResultsViewer.ascx.cs" Inherits="AmazinBargin.userControls.ResultsViewer" %>
<%@ Register Src="~/userControls/pageSelector.ascx" TagPrefix="uc1" TagName="pageSelector" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div>
    <div id="results" class="resultsViewer" runat="server">



    </div>
    <div>
        <uc1:pageSelector runat="server" id="pageSelector" />
    </div>
</div>