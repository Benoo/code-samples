﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class ForumSelection : System.Web.UI.UserControl
    {
        int maxDealsPerPage = 10;
        string id = "";
        protected void Page_Load(object sender, EventArgs e)
        {

        }    

        public void loadCatergoriesForums(string id, string page)
        {
            var Fcat = new Function.Catergory();
            this.id = id;
            loadForumCatergories.InnerHtml = "";
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if (cokkie != null)
            {
                Button1.Text = "Add New Forum Topic";
                Button1.Visible = true;
            }
            else
            {
                Button1.Visible = false;
            }

            if (id == null || id == "")
            {
                DealsTitle.Text = "Forum Catergories";
              

                loadSelectedCatergories(Fcat.getAllCatergorys().Where(x=>x.DealOrForum == true).ToList(), page);
            }
            else
            {
                loadForumsFromCatergoryNullDeal(Fcat.getAllCatergorys().Where(x => x.CatergoryID.ToString() == id).FirstOrDefault());
            }
        }

        private void loadForumsFromCatergoryNullDeal(Models.Catergory cat)
        {
            DealsTitle.Text = cat.CatergoryName;
            var function = new Function.Forum();
            var forums = function.getAllForums().Where(y => y.DateOfFounding > DateTime.Now.AddMonths(-3) && y.DealID == null).Reverse().ToList();         
            loadModuleForums(cat, forums);
        }
        private void loadModuleForums(Models.Catergory cat, List<Models.Forum> forums)
        {
            var function = new Function.Forum();
            var Fposts = new Function.ForumPost();
            var FCat = new Function.Catergory();
            var Fusers = new Function.User();
            var users = Fusers.getAllUsers();
            var cats = FCat.getAllCatergorys();
            var posts = Fposts.getAllForumPosts();

            List<Models.Forum> totalForums = new List<Models.Forum>();

            foreach (var forum in forums)
            {
                var postcount = posts.Count(x => x.ForumID == forum.ForumID);
                var Gcat = cats.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault();
                if (Gcat == null)
                {
                    continue;
                }
                if (cat.CatergoryID != Gcat.CatergoryID)
                {
                    continue;
                }
                totalForums.Add(forum);
            }

            forums = totalForums;
            string Spage = Request.QueryString["page"];
            int page = 0;
            try
            {
                page = int.Parse(Spage);
            }
            catch
            {
                //Not a valid page
            }
            try
            {
                forums = forums.Skip(page * maxDealsPerPage).ToList();
            }
            catch
            {
                //Not Enough Deals
                var x = 1;
            }
            if (forums.Count > maxDealsPerPage)
            {
                forums = forums.Take(maxDealsPerPage).ToList();
            }

            loadForumCatergories.InnerHtml = "";
            string template = "";
            var design = new DealDesignHTML("Forums");
            List<string> d = new List<string>();
            foreach (var forum in forums)
            {              
                var postcount = posts.Count(x => x.ForumID == forum.ForumID);
                var Gcat = cats.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault();
                if (Gcat == null)
                {
                    continue;
                }
                if (cat.CatergoryID != Gcat.CatergoryID)
                {
                    continue;
                }

                template += "";
                template += design.createDealHTML(null, forum, postcount, cat, users.Where(y => y.UserID == forum.FoundingUserID).FirstOrDefault());
                template += "<br />";
            }
            loadForumCatergories.InnerHtml = template;

            if (id == "")
            {
                return;
            }
            var functions = new Function.Deal();
            pageSelector.numberResults(totalForums.Count, maxDealsPerPage, page, "Forums.aspx?category=" + id, true);

        }

        //Deals Functions
        public void loadCatergories(string id, string page)
        {
            string cokkie = null;
            this.id = id;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if (cokkie != null)
            {
                Button1.Text = "Add New Deal";
                Button1.Visible = true;
            }
            else
            {
                Button1.Visible = false;
            }

            var Fcat = new Function.Catergory();
            loadForumCatergories.InnerHtml = "";
            if (id == null || id == "")
            {
                DealsTitle.Text = "Deal Catergories";              

                loadSelectedCatergories(Fcat.getAllCatergorys().Where(x => x.DealOrForum == false).ToList(), page);
            }else
            {
                loadForumsFromCatergory(Fcat.getAllCatergorys().Where(x => x.CatergoryID.ToString() == id).FirstOrDefault());
            }
        }

        public void loadSelectedCatergories(List<Models.Catergory> catergories, string page)
        {
            string html = "";
            var Design = new CatergoryDesignHTML(page);
            foreach(var cat in catergories)
            {
                html += Design.createCatergoyies(cat);
            }
            loadForumCatergories.InnerHtml = html;
        }

        public void loadForumsFromCatergory(Models.Catergory cat)
        {
            DealsTitle.Text = cat.CatergoryName;
            var function = new Function.Deal();
            var deals = function.getAllDeals().Where(y => y.DateStart > DateTime.Now.AddMonths(-3)).Reverse().ToList();          
            loadModule(deals,cat);
        }

        private void loadModule(List<Models.Deal> Deals, Models.Catergory cat)
        {
            //Get the Forums
            var function = new Function.Forum();
            var Fposts = new Function.ForumPost();
            var FCat = new Function.Catergory();
            var Fusers = new Function.User();
            var users = Fusers.getAllUsers();
            var cats = FCat.getAllCatergorys();
            var posts = Fposts.getAllForumPosts();

            var forums = function.getAllForums();

            loadForumCatergories.InnerHtml = "";
            string template = "";
            var design = new DealDesignHTML("Deals");
            List<string> d = new List<string>();
            List<Models.Deal> selectDeal = new List<Models.Deal>();

            foreach (var deala in Deals)
            {
                var foruma = forums.Where(x => x.DealID == deala.DealID).FirstOrDefault();
                if (foruma == null)
                {
                    continue;
                }
                var postcount = posts.Count(x => x.ForumID == foruma.ForumID);
                var Gcat = cats.Where(x => x.CatergoryID == foruma.CategoryID).FirstOrDefault();
                if (Gcat == null)
                {
                    continue;
                }
                if (cat.CatergoryID != Gcat.CatergoryID)
                {
                    continue;
                }
                selectDeal.Add(deala);
            }


            ///////////////////Filter Results
            Deals = selectDeal;
            string Spage = Request.QueryString["page"];
            int page = 0;
            try
            {
                page = int.Parse(Spage);
            }
            catch
            {
                //Not a valid page
            }
            try
            {
                Deals = Deals.Skip(page * maxDealsPerPage).ToList();
            }
            catch
            {
                //Not Enough Deals
                var x = 1;
            }
           
            if (selectDeal.Count > maxDealsPerPage)
            {
                Deals = Deals.Take(maxDealsPerPage).ToList();
            }
            /////////////////////////


            foreach (var deal in Deals)
            {
                var forum = forums.Where(x => x.DealID == deal.DealID).FirstOrDefault();
                if(forum == null)
                {
                    continue;
                }
                var postcount = posts.Count(x => x.ForumID == forum.ForumID);
                var Gcat = cats.Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault();
                if(Gcat == null)
                {
                    continue;
                }
                if(cat.CatergoryID != Gcat.CatergoryID)
                {
                    continue;
                }

                template += "";
                template += design.createDealHTML(deal, forum, postcount, cat, users.Where(y => y.UserID == forum.FoundingUserID).FirstOrDefault());            
                template += "<br />";
            }
            loadForumCatergories.InnerHtml = template;
                           
            if(id == "")
            {
                return;
            }
            var functions = new Function.Deal();
            pageSelector.numberResults(selectDeal.Count, maxDealsPerPage, page,"Deals.aspx?category="+id,true);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if(Button1.Text == "Add New Deal")
            {
                Response.Redirect("AddNew.aspx?add=deal");
                return;
            }
            if(Button1.Text == "Add New Forum Topic")
            {
                Response.Redirect("AddNew.aspx?add=forum");
                return;
            }
        }
    }
}