﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="accountActivity.ascx.cs" Inherits="AmazinBargin.userControls.accountActivity" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="account" class="account" runat="server">
    <asp:Label ID="EditAccount" runat="server" Text="Profile" Font-Size="Large"></asp:Label><br />
    <br /><br />
    <div id="loadAccountInformation" runat="server">
        Username:&nbsp;&nbsp;  <asp:Label ID="Label1" runat="server" Text="Username"></asp:Label><br />
        &nbsp;Join Date:&nbsp;&nbsp;  <asp:Label ID="Label2" runat="server" Text="10 Jan 2019"></asp:Label><br />
        &nbsp;
        Location:&nbsp;&nbsp;  <asp:Label ID="Label3" runat="server" Text="New York"></asp:Label>
        <br />
        <br />
        <br />
        <br />  
        <asp:Button ID="Button1" runat="server" Text="Edit My Account"  CssClass="normalButton" OnClick="Button1_Click"/>
        <br /><br />
        <div style="font-size:18px;">My Latest Posts & Deals</div>
        <div id="loadUsersActivities" runat="server">


        </div>
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" Text="Log out"  CssClass="normalButton" OnClick="Button2_Click" />
    </div>
    <div id="moderatorSection" runat="server" visible="false">
        <br />Moderator<br />
        <br />
        Ban User<br />
        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList><br />
        Until<br />
        <asp:TextBox ID="TextBox1" runat="server" TextMode="Date"></asp:TextBox><br />
        <br />Abuse of Power will not be tolerated
        <br />All Bans must have a reason<br />
        <asp:Button ID="Button3" runat="server" Text="Ban" CssClass="normalButton" Width="70px" OnClick="Button3_Click" />
        <asp:HiddenField ID="ModUserID" runat="server" /><br /><br /><br />
        Change User Ranking<br />
        <asp:DropDownList ID="DropDownList2" runat="server"></asp:DropDownList><br />
        Change Ranking To<br />
        <asp:DropDownList ID="DropDownList3" runat="server"></asp:DropDownList><br /><br />
         <asp:Button ID="Button4" runat="server" Text="Change Ranking" CssClass="normalButton" Width="110px" OnClick="Button4_Click" />
    </div>
</div>