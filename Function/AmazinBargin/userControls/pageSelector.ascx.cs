﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class pageSelector : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void numberResults(int results, int numberperPage, int pageSelected, string filename = "", bool multiple = false)
        {
            results = ((int)Math.Ceiling((decimal)results / (decimal)numberperPage))*numberperPage;
            selector.InnerHtml = "";
            string html = "";
            if (filename == "")
            {
                filename = Path.GetFileName(Request.Path);
            }

            var pagesel = new PageSelectorHTML(pageSelected, "../" + filename,multiple);
            int numberOfRows = 4;
            if (pageSelected != 0)
            {
                html += pagesel.getBackButton();
            }
            if (pageSelected > numberOfRows)
            {
                html += pagesel.getNothingButton();
            }

            for(int i = -numberOfRows; i <= numberOfRows; i++)
            {
                if(pageSelected + i < 0)
                {
                    continue;
                }
                if (pageSelected + i >= (results / numberperPage))
                {
                    break;
                }
                if(i== 0)
                {
                    html += pagesel.getNumberButtonSelect(pageSelected + i);
                }
                else
                {
                    html += pagesel.getNumberButton(pageSelected + i);
                }
           
            }


            //Inbetween Buttons
            if (pageSelected < (results / numberperPage) -2)
            {
                html += pagesel.getNothingButton();
            }
            if (pageSelected < (results / numberperPage)-1)
            {           
                html += pagesel.getFowardButton();
            }

            selector.InnerHtml = html;
        }
    }
}