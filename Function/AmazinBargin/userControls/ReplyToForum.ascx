﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReplyToForum.ascx.cs" Inherits="AmazinBargin.userControls.ReplyToForum" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="reply" class="replytoForum" runat="server" style="min-width:200px;">
    Reply<br />
    <br />
    <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol" Height="192px" TextMode="MultiLine"></asp:TextBox><br />
    <asp:Button ID="Button1" runat="server" Text="Submit Reply"  CssClass="normalButton" OnClick="Button1_Click"/>
</div>
<div id="notloggedin" class="replytoForum" runat="server" visible="false">
    To reply, please log in. If you don't have an account you can create one by visiting 'My Account'
</div>