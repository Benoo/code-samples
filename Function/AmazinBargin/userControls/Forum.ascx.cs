﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class Forum : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var a = HiddenField1.Value;
            if(a != "" || a != null)
            {
                deletecomment();
            }
        }

        public void LoadForum(int forumID)
        {
            dealTeaseTitle.InnerHtml = "";
            Username.Text = "";
            dealDescription.InnerText = "";
            Category.Text = "";
            Date.Text = "";
            Code.Text = "";
            dealimage.Visible = false;
            var Fforums = new Function.Forum();
            var Fusers = new Function.User();                   
            var forum = Fforums.getAllForums().Where(x => x.ForumID == forumID).FirstOrDefault();
            if(forum == null)
            {
                //There was an error
                return;
            }
            var user = Fusers.getAllUsers().Where(x => x.UserID == forum.FoundingUserID).FirstOrDefault();
            if(user == null)
            {
                //No user error
                return;
            }
            if (user == null)
            {
                return;
            }
            bool mod = false;
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            var Cuser = Fusers.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
            if (Cuser != null)
            {
                if (Cuser.UserTypeID == 2)
                {
                    ModUserID.Value = user.UserID.ToString();
                    moderator.Visible = true;
                    mod = true;
                }
                else
                {
                    moderator.Visible = false;
                }
            }else
            {
                vote.Visible = false;
            }
            dealstats.Visible = false;
           
            if (forum.DealID != null)
            {
                var FDeals = new Function.Deal();
                var deal = FDeals.getAllDeals().Where(x => x.DealID == forum.DealID).FirstOrDefault();
                if(deal != null)
                {
                    Code.Text = "Code: " + deal.CodeWord;
                    dealstats.Visible = true;                   
                    var FUserDeals = new Function.UserDealLike();
                    var UserDeals = FUserDeals.getAllUserDealLikes();
                    int votes = UserDeals.Where(x => x.DealID == deal.DealID && x.LikedDeal == true).Count();
                    Label1.Text = "Votes: " + votes;
                    if(deal.DealImage != "" && deal.DealImage != null)
                    {
                        dealimage.Visible = true;
                        Image1.ImageUrl = "~/" + deal.DealImage;
                        HyperLink1.NavigateUrl = "~/" + forum.WebsiteLink;
                    }

                }
            }

            var Fcat = new Function.Catergory();
            var cat = Fcat.getAllCatergorys().Where(x => x.CatergoryID == forum.CategoryID).FirstOrDefault();
            Username.Text = user.Username;
            dealDescription.InnerText = forum.Description;
            Date.Text = forum.DateOfFounding.ToShortDateString();
            if(cat != null)
            {
                Category.Text = cat.CatergoryName;
            }
            
            var name = forum.ForumName.Split(' ');
            string fname = "";
            foreach (var n in name)
            {
                if (n.Contains('$'))
                {
                    string before = "";
                    string after = "";
                    string required = "$1234567890,.";
                    for (var i = 0; i < n.Count(); i++)
                    {
                        var c = n[i];
                        if (required.Contains(c))
                        {
                            before += c;
                        }
                        else
                        {
                            after += c;
                        }
                    }
                    fname += "<em class=\"money\">" + before + "</em>" + after + " ";
                }
                else
                {
                    fname += n + " ";
                }
            }
            dealTeaseTitle.InnerHtml = fname;

            var FPosts = new Function.ForumPost();
            var posts = FPosts.getAllForumPosts().Where(x => x.ForumID == forumID).ToList();
            var genComments = new CommentsDesignHTML();
            var FLikes = new Function.UserLikesAndDislike();
            var likes = FLikes.getAllUserLikesAndDislikes();
            var users = Fusers.getAllUsers();
            string html = "";
            foreach(var post in posts)
            {
                var like = likes.Where(x => x.PostID == post.PostID).ToList();
                int votes = 0;
                foreach (var li in like)
                {
                    if(li.LikedPost == null)
                    {                      
                        continue;
                    }
                    if (li.LikedPost ?? false)
                    {
                        votes++;
                    }else
                    {
                        votes--;
                    }          
                }
                html += genComments.createComments(post, users.Where(x => x.UserID == post.UserID).FirstOrDefault(), votes);
                if (mod)
                {
                    html += genComments.deleteComment(post);
                }
            }
            forumsComments.InnerHtml = html;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //Add vote
            string cokkie = null;
            try
            {
                cokkie = Request.Cookies["UserSettings"]["User"];
            }
            catch { }
            if (cokkie == null)
            {
                return;
            }
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Username == cokkie).FirstOrDefault();
         
            //Get the forum
            string node = Request.QueryString["node"];
            var Fforums = new Function.Forum();
            var forum = Fforums.getAllForums().Where(x => x.ForumID.ToString() == node).FirstOrDefault();
            if (forum == null)
            {
                return;
            }

            //Okay find the deal
            if (forum.DealID == null)
            {
                return;
            }
            var FDeals = new Function.Deal();
            var deal = FDeals.getAllDeals().Where(x => x.DealID == forum.DealID).FirstOrDefault();
            if (deal == null)
            {
                return;
            }

            //Check to see if the user has already voted. if so invert their vote 
            //else add their vote
            var FUserDeals = new Function.UserDealLike();
            var UserDeal = FUserDeals.getAllUserDealLikes().Where(x=>x.UserID == user.UserID && x.DealID == deal.DealID).FirstOrDefault();
            if(UserDeal == null)
            {
                UserDeal = new Models.UserDealLike(
                    0,
                    user.UserID,
                    deal.DealID,
                    true);
            }else
            {
                if (UserDeal.LikedDeal ?? false)
                {
                    UserDeal.LikedDeal = false;
                }else
                {
                    UserDeal.LikedDeal = true;
                }
            }

            FUserDeals.saveUserDealLike(UserDeal);
            LoadForum(forum.ForumID);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            var fUser = new Function.User();
            var id = -1;
            try
            {
                id = int.Parse(ModUserID.Value);
            }
            catch
            {
                return;
            }
            var user = fUser.getUsers(id);
            if (user == null)
            {
                return;
            }
            if (user.UserTypeID != 2)
            {
                return;
            }
            string node = Request.QueryString["node"];
            var Fforums = new Function.Forum();
            var forum = Fforums.getAllForums().Where(x => x.ForumID.ToString() == node).FirstOrDefault();
            if (forum == null)
            {
                return;
            }
            Fforums.deleteForum(forum);
            Response.Redirect("Home.aspx");
        }

        protected void deletecomment()
        {
            //Delete Selected Comment
            int Postvalue = -1;
            try
            {
                Postvalue = int.Parse(HiddenField1.Value);
            }
            catch
            {
                return;
            }
            
            var fUser = new Function.User();
            var id = -1;
            try
            {
                id = int.Parse(ModUserID.Value);
            }
            catch
            {
                return;
            }
            var user = fUser.getUsers(id);
            if (user == null)
            {
                return;
            }
            if (user.UserTypeID != 2)
            {
                return;
            }
            string node = Request.QueryString["node"];
            var Fforums = new Function.Forum();
            var forum = Fforums.getAllForums().Where(x => x.ForumID.ToString() == node).FirstOrDefault();
            if (forum == null)
            {
                return;
            }

            //Everything valid so far lets get the post
            var Fposts = new Function.ForumPost();
            var post = Fposts.getForumPosts(Postvalue);
            if(post == null)
            {
                return; //That post doesn't exist
            }
            if (post.ForumID == forum.ForumID)
            {
                Fposts.deleteForumPost(post);//All good to delete
                ModUserID.Value = "";
                Response.Redirect(Request.RawUrl);
            }              
        }
    }
}