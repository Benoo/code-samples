﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateAccount.ascx.cs" Inherits="AmazinBargin.userControls.CreateAccount" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="account" class="account" runat="server">
    <asp:Label ID="EditAccount" runat="server" Text="Create Account"></asp:Label><br />
    <div style="font-size:10px;">If you wish to create deals and reply to posts you will need to create an account.</div>
    <br /><br />
    <div id="loadAccountInformation" runat="server">
        Username* <br />
        <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol"></asp:TextBox><br />
        <div style="font-size:10px;">Please choose carefully as your username can't be changed later</div>
        <br />
        Email* <br />
        <asp:TextBox ID="TextBox2" runat="server" CssClass="textboxcontrol" TextMode="Email"></asp:TextBox><br />
        <div style="font-size:10px;">You will need to verify your email address before you can use your account </div><br />
        Password* <br />
        <asp:TextBox ID="TextBox3" runat="server" CssClass="textboxcontrol" TextMode="Password"></asp:TextBox><br /><br />
        Location* <br />
        <asp:TextBox ID="TextBox5" runat="server" CssClass="textboxcontrol"></asp:TextBox><br />
        <div style="font-size:10px;">Where you are (Cities: New York, London, Tokyo, ect) </div>
        <br />
        Gender<br />
        <asp:DropDownList ID="DropDownList1" runat="server">
            <asp:ListItem>Prefer Not Say / Other</asp:ListItem>
            <asp:ListItem Value="Male"></asp:ListItem>
            <asp:ListItem>Female</asp:ListItem>
        </asp:DropDownList><br /><br />

        <asp:Button ID="Button1" runat="server" Text="Create My Account"  CssClass="normalButton" OnClick="Button1_Click"/>
        

    </div>
</div>