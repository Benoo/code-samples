﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class Menu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var colour = new Color();
            colour = Color.FromArgb(10, 16, 40);

            string title = Page.Title;
            switch (title)
            {
                case "Home":
                    
                    break;
                case "Deals":
                    Button2.BackColor = colour;
                    break;
                case "Forums":
                    Button3.BackColor = colour;
                    break;
                case "My Account":
                    Button4.BackColor = colour;
                    break;
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("Home.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("Deals.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("Forums.aspx");
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("MyAccount.aspx");
        }    
    }
}