﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Function;

namespace AmazinBargin.userControls
{
    public partial class ResultsViewer : System.Web.UI.UserControl
    {
        int maxDealsPerPage = 10;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void loadResults(string searchString)
        {
            
        }

        public void loadResultsHomePage()
        {
            var function = new Function.Deal();
            var deals = function.getAllDeals().Where(y => y.DateStart > DateTime.Now.AddMonths(-1)).Reverse().ToList();
            string Spage = Request.QueryString["page"];
            int page = 0;
            try
            {
                page = int.Parse(Spage);
            }
            catch
            {
                //Not a valid page
            }
            try
            {
                deals = deals.Skip(page * maxDealsPerPage).ToList();                
            }
            catch
            {
                //Not Enough Deals
                var x = 1;
            }
            if (deals.Count > maxDealsPerPage)
            {
                deals = deals.Take(maxDealsPerPage).ToList();
            }
            loadModule(deals);
        }

        private void loadModule(List<Models.Deal> Deals)
        {
            //Get the Forums
            var function = new Function.Forum();
            var Fposts = new Function.ForumPost();
            var FCat = new Function.Catergory();
            var Fusers = new Function.User();
            var users = Fusers.getAllUsers();
            var cats = FCat.getAllCatergorys();
            var posts = Fposts.getAllForumPosts();

            var forums = function.getAllForums();

            results.InnerHtml = "";
            string template = "";
            var design = new DealDesignHTML("Deals");
            List<string> d = new List<string>();
            foreach(var deal in Deals)
            {
                var forum = forums.Where(x => x.DealID == deal.DealID).FirstOrDefault();
                if(forum == null)
                {
                    continue;
                }
                var postcount = posts.Count(x => x.ForumID == forum.ForumID);
                     
                template += "";
                template += design.createDealHTML(deal, forum, postcount, cats.Where(x=>x.CatergoryID == forum.CategoryID).FirstOrDefault(),users.Where(y=>y.UserID == forum.FoundingUserID).FirstOrDefault());
                template += "<br />";
            }
            results.InnerHtml = template;
            int page = 0;
            try
            {
                page = int.Parse(Request.QueryString["page"]);
            }
            catch { }
            var functions = new Function.Deal();
            var deals = functions.getAllDeals().Where(y => y.DateStart > DateTime.Now.AddMonths(-1)).Count();
            pageSelector.numberResults(deals, maxDealsPerPage, page);
        }
    }
}