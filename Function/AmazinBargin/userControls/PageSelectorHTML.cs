﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazinBargin.userControls
{
    public class PageSelectorHTML
    {
        int page;
        string addition;
        string multi = "?";
        public PageSelectorHTML(int page,string addtion,bool multiple = false)
        {
            this.page = page;
            this.addition = addtion;
            if (multiple)
            {
                multi = "&";
            }
        }
        public string getFowardButton()   
        {
            return "<div class=\"pageSelectorButton\"> <a href = \""+ addition + multi + "page=" + (page + 1).ToString() + "\"> --> </a></div>";
        }
        public string getBackButton()
        {
            return "<div class=\"pageSelectorButton\"> <a href = \"" + addition + multi + "page=" + (page - 1).ToString() + "\"> <-- </a></div>";
        }
        public string getNothingButton()
        {
            return "<div class=\"pageSelectorButton\"> ... </div>";
        }
        public string getNumberButton(int page)
        {
            return "<div class=\"pageSelectorButton\"> <a href = \"" + addition + multi + "page=" + (page).ToString() + "\"> "+ (page).ToString() + " </a></div>";
        }
        public string getNumberButtonSelect(int page)
        {
            return "<div class=\"pageSelectorButtonSelected\"> <a href = \"" + addition + multi + "page=" + (page).ToString() + "\"> " + (page).ToString() + " </a></div>";
        }
    }
}