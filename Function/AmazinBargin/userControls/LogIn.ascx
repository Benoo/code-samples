﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LogIn.ascx.cs" Inherits="AmazinBargin.userControls.LogIn" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="account" class="account" runat="server">
    <asp:Label ID="EditAccount" runat="server" Text="Log In"></asp:Label><br />
    <br />
    <div id="loadAccountInformation" runat="server">
        Username <br />
        <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol"></asp:TextBox><br />      
        <br />  
        Password <br />
        <asp:TextBox ID="TextBox3" runat="server" CssClass="textboxcontrol" TextMode="Password"></asp:TextBox><br /><br />

        <asp:Button ID="Button1" runat="server" Text="Log In"  CssClass="normalButton" OnClick="Button1_Click"/>
        <br /><br />
         <div style="font-size:10px;">Or, I don't have an account <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Create Account</asp:LinkButton> </div><br />
         <div style="font-size:10px;">Or, I forgot my username & password <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click" >Recover Account</asp:LinkButton> </div>
    </div>
</div>