﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin.userControls
{
    public partial class CreateAccount : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            checkDetails();
        }

        protected void checkDetails()
        {
            string username = TextBox1.Text;
            string email = TextBox2.Text;
            string password = TextBox3.Text;
            bool ret = false;
            TextBox1.BackColor = System.Drawing.Color.White;
            TextBox2.BackColor = System.Drawing.Color.White;
            TextBox3.BackColor = System.Drawing.Color.White;

            if (username == null || username == "" || username.Contains('<') || username.Contains('>'))
            {
                TextBox1.Text = "";
                TextBox1.BackColor = System.Drawing.Color.Red;
                ret = true;
            }
            if (email == null || email == "" || email.Contains('<') || email.Contains('>') || !email.Contains('@'))
            {
                TextBox2.Text = "";
                TextBox2.BackColor = System.Drawing.Color.Red;
                ret = true;
            }
            if (password == null || password == "" || password.Count() < 7)
            {
                TextBox3.Text = "";
                TextBox3.BackColor = System.Drawing.Color.Red;
                ret = true;
            }



            var Fusers = new Function.User();
            var user = Fusers.getAllUsers().Where(x => x.Username.ToLower() == username.ToLower()).FirstOrDefault();
            if(user != null)
            {
                TextBox1.Text = "";
                TextBox1.BackColor = System.Drawing.Color.Red;
                ret = true;
            }
            var aemail = Fusers.getAllUsers().Where(x => x.Email.ToLower() == email.ToLower()).FirstOrDefault();
            if (aemail != null)
            {
                TextBox2.Text = "";
                TextBox2.BackColor = System.Drawing.Color.Red;
                ret = true;
            }
            if (ret)
            {
                return;
            }
            createAccount();
        }

        protected void createAccount()
        {
            string username = TextBox1.Text;
            string email = TextBox2.Text;
            string password = TextBox3.Text;
            string location = TextBox5.Text;
            string gender = null;
            if (DropDownList1.SelectedIndex > -1)
            {
                gender = DropDownList1.SelectedItem.Text;
            }

            var user = new Models.User(
                0,
                username,
                email,
                password,
                "",
                gender,
                location,
                "",
                DateTime.Now,
                null,
                false,
                1,
                DateTime.Now,
                ""
            );

            var Fusers = new Function.User();
            Fusers.saveUser(user);          
            var FRecovery = new Function.PasswordRecovery();
            var recover = FRecovery.getPasswordRecoverys(user.UserID);
            if (recover == null)
            {
                recover = new Models.PasswordRecovery(user.UserID, FRecovery.getRandomKeyWithPrefix("verify"), DateTime.Now);
            }
            else
            {
                recover.RecoveryKey = FRecovery.getRandomKeyWithPrefix("verify");
                recover.RecoveryRequested = DateTime.Now;
            }
            FRecovery.savePasswordRecovery(recover);

            var mail = new MailService.SendEmail();
            mail.sendemailVerifyUser(user, Request.Url.ToString().Substring(0, Request.Url.ToString().LastIndexOf('/')) + "/" + "Verify.aspx?key=" + recover.RecoveryKey);
            Response.Redirect("MyAccount.aspx?create=verify");
        }
    }
}