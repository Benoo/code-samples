﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class _404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check to see if it is an external link in the db and redirect
            string path = Request.QueryString["aspxerrorpath"] ?? Request.RawUrl;
            var Fwebsite = new Function.Website();
            var websites = Fwebsite.getAllWebsites();
            var website = websites.Where(x => path.Contains(x.WebsiteLink)).FirstOrDefault();
            if(website != null)
            {
                Response.Redirect(website.WebsiteName);
            }
        }
    }
}