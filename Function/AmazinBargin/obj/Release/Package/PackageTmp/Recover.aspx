﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Recover.aspx.cs" Inherits="AmazinBargin.Recover" %>
<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/emailControls/RecoverPassword.ascx" TagPrefix="uc1" TagName="RecoverPassword" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>



<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <uc1:Menu runat="server" ID="Menu" />
         <br />
        <div class="maincontent">
             <div class="dealTeaseTitle">
                 Recover Account
            </div><br />
            <br />
            <div id="Recovery" runat="server" visible="false">
                <uc1:RecoverPassword runat="server" id="RecoverPassword" />
            </div>  
        </div>
         <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
