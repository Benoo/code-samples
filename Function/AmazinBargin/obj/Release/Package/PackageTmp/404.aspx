﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="AmazinBargin._404" %>

<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>

<!DOCTYPE html>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Not Found</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <br />
        <div style="text-align:center">
            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Home.aspx" ImageUrl="~/Images/logobig.png">
            </asp:HyperLink>
            <div class="dealTeaseTitle">
                 404 - Page Not Found
            </div><br />
            The page you are looking for could not be found.<br />
            <br />
            <div style="margin-left:6%;">
            <uc1:SideBar runat="server" ID="SideBar" />
                </div>
         </div>     
    </div>
    </form>
</body>
</html>
