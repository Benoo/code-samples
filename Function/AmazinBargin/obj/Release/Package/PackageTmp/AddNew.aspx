﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddNew.aspx.cs" Inherits="AmazinBargin.AddNew" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/AddNewControls/NewDeal.ascx" TagPrefix="uc1" TagName="NewDeal" %>
<%@ Register Src="~/AddNewControls/NewForum.ascx" TagPrefix="uc1" TagName="NewForum" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>





<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add New</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Menu runat="server" id="Menu" />
        <br />
        <div class="maincontent">
            <div id="deal" runat="server" visible ="false">
                <uc1:NewDeal runat="server" id="NewDeal" />
            </div>
            <div id="forum" runat="server" visible="false">
                <uc1:NewForum runat="server" id="NewForum" />
            </div>
        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>      
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
