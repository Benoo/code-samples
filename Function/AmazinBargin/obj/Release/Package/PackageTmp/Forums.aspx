﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Forums.aspx.cs" Inherits="AmazinBargin.Forums" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/ForumSelection.ascx" TagPrefix="uc1" TagName="ForumSelection" %>
<%@ Register Src="~/userControls/Forum.ascx" TagPrefix="uc1" TagName="Forum" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>






<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forums</title>
</head>
<body>
    <form id="form1" runat="server">
     <div>
       <uc1:Menu runat="server" ID="Menu" />
        <br />
        <div class="maincontent">
            <div id="forumselectiondiv" runat="server" visible="false">
                <uc1:ForumSelection runat="server" ID="ForumSelection" />
            </div>
            <div id="forumdiv" runat="server" visible="false">
                <uc1:Forum runat="server" ID="Forum" />
            </div>
        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div> 
         <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
