﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="AmazinBargin.Home" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/ResultsViewer.ascx" TagPrefix="uc1" TagName="ResultsViewer" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>





<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Amazin Bargins</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Menu runat="server" id="Menu" />
        <br />
        <div class="maincontent">
             <uc1:ResultsViewer runat="server" id="ResultsViewer" />
        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div>
        <uc1:footer runat="server" id="footer" />
    </div>
    </form>
</body>
</html>
