﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewDeal.ascx.cs" Inherits="AmazinBargin.AddNewControls.NewDeal" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="forum" class="forum" runat="server">
    <asp:Label ID="DealsTitle" runat="server" Text="Create New Deal" CssClass="dealTeaseTitle"></asp:Label><br />
    <br />
    <div id="forums" runat="server">
        Subject*<br />
        <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
        Deal Category*<br />
        <asp:DropDownList ID="DropDownList1" runat="server"></asp:DropDownList><br />
        <br />
        Deal Image<br />
        <asp:FileUpload ID="FileUpload1" runat="server"/>
        <br />
        <br />
        Image Website Link<br />
        <asp:TextBox ID="TextBox7" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
        Code<br />
        <asp:TextBox ID="TextBox3" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
        Start Date<br />
        <asp:TextBox ID="TextBox4" runat="server" CssClass="textboxcontrol" TextMode="Date"></asp:TextBox><br /><br />
        End Date<br />
        <asp:TextBox ID="TextBox5" runat="server" CssClass="textboxcontrol" TextMode="Date"></asp:TextBox><br /><br />
        Savings Amount ($)<br />
        <asp:TextBox ID="TextBox6" runat="server" CssClass="textboxcontrol" ></asp:TextBox><br /><br />


        Body*<br />
        <asp:TextBox ID="TextBox2" runat="server" CssClass="textboxcontrol" Height="305px" TextMode="MultiLine"></asp:TextBox><br /><br />
    </div>
    <br />
    <asp:Button ID="Button1" runat="server" Text="Submit Deal"  CssClass="normalButton" OnClick="Button1_Click"/>
</div>