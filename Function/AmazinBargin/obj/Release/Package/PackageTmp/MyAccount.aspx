﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyAccount.aspx.cs" Inherits="AmazinBargin.MyAccount" %>

<%@ Register Src="~/userControls/Menu.ascx" TagPrefix="uc1" TagName="Menu" %>
<%@ Register Src="~/userControls/editAccountDetails.ascx" TagPrefix="uc1" TagName="editAccountDetails" %>
<%@ Register Src="~/userControls/CreateAccount.ascx" TagPrefix="uc1" TagName="CreateAccount" %>
<%@ Register Src="~/userControls/LogIn.ascx" TagPrefix="uc1" TagName="LogIn" %>
<%@ Register Src="~/userControls/accountActivity.ascx" TagPrefix="uc1" TagName="accountActivity" %>
<%@ Register Src="~/userControls/CreateVerify.ascx" TagPrefix="uc1" TagName="CreateVerify" %>
<%@ Register Src="~/userControls/SideBar.ascx" TagPrefix="uc1" TagName="SideBar" %>
<%@ Register Src="~/userControls/footer.ascx" TagPrefix="uc1" TagName="footer" %>









<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Account</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <uc1:Menu runat="server" ID="Menu" />
        <br />
        <div class="maincontent">
           <div id="editAccount" runat="server" visible="false">
               <uc1:editAccountDetails runat="server" id="editAccountDetails" />
           </div>
           <div id="createAAccount" runat="server" visible="false">
               <uc1:CreateAccount runat="server" id="CreateAccount" />
           </div>
           <div id="login" runat="server" visible="false">
               <uc1:LogIn runat="server" id="LogIn1" />
           </div>
           <div id="accountsActivity" runat="server" visible="false">
               <uc1:accountActivity runat="server" id="accountActivity" />
           </div>  
           <div id="verify" runat="server" visible="false">
               <uc1:CreateVerify runat="server" id="CreateVerify" />
           </div>         
        </div>
        <div class ="sideMenu">
            <uc1:SideBar runat="server" id="SideBar" />
        </div> 
        <uc1:footer runat="server" ID="footer" />
    </div>
    </form>
</body>
</html>
