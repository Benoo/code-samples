﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="editAccountDetails.ascx.cs" Inherits="AmazinBargin.userControls.editAccountDetails" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<div id="account" class="account" runat="server">
    <asp:Label ID="EditAccount" runat="server" Text="Account Infomation"></asp:Label><br />
    <br />
    <div id="loadAccountInformation" runat="server">
        Username <br />
        <asp:TextBox ID="TextBox1" runat="server" CssClass="textboxcontrol" ReadOnly="True"></asp:TextBox><br /><br />
        Email <br />
        <asp:TextBox ID="TextBox2" runat="server" CssClass="textboxcontrol" ReadOnly="True"></asp:TextBox><br /><br />
        Password <br />
        <asp:TextBox ID="TextBox3" runat="server" CssClass="textboxcontrol" TextMode="Password" ToolTip="Old Password"></asp:TextBox><br />
        <asp:TextBox ID="TextBox4" runat="server" CssClass="textboxcontrol" TextMode="Password" ToolTip="New Password"></asp:TextBox><br /><br />
        Location <br />
        <asp:TextBox ID="TextBox5" runat="server" CssClass="textboxcontrol"></asp:TextBox><br /><br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Submit"  CssClass="normalButton" OnClick="Button1_Click"/>
    </div>
</div>