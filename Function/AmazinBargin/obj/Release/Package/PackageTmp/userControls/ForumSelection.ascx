﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ForumSelection.ascx.cs" Inherits="AmazinBargin.userControls.ForumSelection" %>
<%@ Register Src="~/userControls/pageSelector.ascx" TagPrefix="uc1" TagName="pageSelector" %>


<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<div id="forum" class="forum" runat="server">
    <asp:Label ID="DealsTitle" runat="server" Text="Deal Catergories"></asp:Label><br />
    <br />
    <div id="loadForumCatergories" runat="server">

    </div>
    <br />
    <uc1:pageSelector runat="server" ID="pageSelector" />
    <br />
    <asp:Button ID="Button1" runat="server" Text="Add A Deal"  CssClass="normalButton" OnClick="Button1_Click"/>
</div>