﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Menu.ascx.cs" Inherits="AmazinBargin.userControls.Menu" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<div class="menu">   
    <asp:Button ID="Button1" runat="server" Text="Amazin Bargins" CssClass="menuLogo" OnClick="Button1_Click"/>
    <asp:Button ID="Button2" runat="server" Text="Deals" CssClass="menuButton" OnClick="Button2_Click"/>
    <asp:Button ID="Button3" runat="server" Text="Forums" CssClass="menuButton" OnClick="Button3_Click"/>
    <asp:Button ID="Button4" runat="server" Text="My Account" CssClass="menuButton" OnClick="Button4_Click"/>
</div>