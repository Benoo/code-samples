﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Forum.ascx.cs" Inherits="AmazinBargin.userControls.Forum" %>
<%@ Register Src="~/userControls/ReplyToForum.ascx" TagPrefix="uc1" TagName="ReplyToForum" %>

<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />

<div id="forum" class="forum" runat="server">
    
    <div class="dealTeaseLeft"></div>
    <div class = "dealTeaseCenter">
        <div id="dealTeaseTitle" class="forumTitle" runat="server">
        This Is the Title Of the Forum <em class="money"> $90.00 </em>
        </div>
        <br />
        by <asp:Label ID="Username" runat="server" Text="Username"></asp:Label>
        <br /><br />
        <div id="dealDescription" class="forumDealDescription" runat ="server">
        Hello. I found this cool deal while doing some things. Here. Go and have a look at this awesome deal. Only
        10,000,000 left so get in quick as stock is limited.
        </div><br />
        <asp:TextBox ID="Code" runat="server" CssClass="code" Text="code" ReadOnly="True">code</asp:TextBox>
        <br /><br />
        <asp:Label ID="Category" runat="server" Text="Category"></asp:Label> 
        &nbsp;<asp:Label ID="Date" runat="server" Text="Date"></asp:Label><br />     
        <div id="moderator" runat="server" visible="false">
            <asp:Button ID="Button2" runat="server" Text="Delete"  CssClass="normalButton" OnClick="Button2_Click"/>
            <div style="visibility:hidden">                   
                <asp:HiddenField ID="HiddenField1" runat="server" />
                 <script>
                     function SetCommentIDAndClick(inti) {
                         document.getElementById("<%=HiddenField1.ClientID %>").value = inti;
                         window.location.reload(true);
                    }
                </script>
               
            </div>          
            <asp:HiddenField ID="ModUserID" runat="server" />
        </div>  
        <br />
        <em class="comments">Comments</em>
        <br />
        <br />
        <br />  
        <div id="forumsComments" runat="server">


        </div>    
        <div id="replyToForum" runat="server">
            <uc1:ReplyToForum runat="server" id="ReplyToForum1" />
        </div>
    </div>
    <div class="dealTeaseRight">
      <div id="dealimage" runat="server">
          <asp:HyperLink ID="HyperLink1" runat="server">
          <asp:Image ID="Image1" runat="server" ImageAlign="Middle" Width="232px" Height="232px" />
          </asp:HyperLink>
      </div>
      <div id="dealstats" runat="server">
          <asp:Label ID="Label1" runat="server" Text="Votes: "></asp:Label><br />
          <div id="vote" runat="server">
             <asp:Button ID="Button1" runat="server" Text="+ Vote"  CssClass="normalButton" OnClick="Button1_Click"/>
          </div>
      </div>

    </div>
</div>