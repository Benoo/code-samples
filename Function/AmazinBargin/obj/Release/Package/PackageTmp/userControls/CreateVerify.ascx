﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CreateVerify.ascx.cs" Inherits="AmazinBargin.userControls.CreateVerify" %>
<link rel="Stylesheet" href="/css/main.css" type="text/css" />
<link rel="Stylesheet" href="/css/controls.css" type="text/css" />
<div id="account" class="account" runat="server">
    <asp:Label ID="EditAccount" runat="server" Text="Verify Account"></asp:Label><br />
    <br /><br />
    <div id="loadAccountInformation" runat="server">
        Thank you for creating an account.
        <br /><br />
        An email will be send to the email address you entered shortly. Please click on the verify my account link to verify your account so you can start
        posting deals.<br />
        <div style="font-size:10px;">Make sure to check your junk folder just in case the verification email gets filtered.</div>
    </div>
</div>