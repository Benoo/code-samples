﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazinBargin
{
    public class ValidateUser
    {
        public bool login(string username, string password,HttpResponse response,string ip)
        {
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Username == username && x.Password == password).FirstOrDefault();
            if(user == null)
            {
                return false;
            }
            if(user.BannedUntil != null)
            {
                if(user.BannedUntil > DateTime.Now)
                {
                    response.Write("<script>alert('You Are BANNED!!!');</script>");
                    return false;
                }
            }
            if(user.VerifiedEmail == false)
            {
                response.Write("<script>alert('Please verify your account by clicking the link in the email that was sent');</script>");
                return false;
            }
            if(user != null)
            {
                response.Cookies["UserSettings"]["User"] = user.Username;
                response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(7d);
                if(user.UsersLastIP != ip)
                {
                    user.UsersLastIP = ip;
                }
                user.LastVisitedDate = DateTime.Now;
                Fuser.saveUser(user);
                return true;
            }
            return false;
        }

        public void logout(HttpResponse response)
        {
            response.Cookies["UserSettings"].Expires = DateTime.Now.AddDays(-1d);          
        }

    }
}