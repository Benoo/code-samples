﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazinBargin
{
    public class CommentsDesignHTML
    {
        public string createComments(Models.ForumPost post, Models.User user, int votes)
        {
            string html = "";
            string firstPart = "<div class=\"forumComment\"><div class=\"forumCommentLeft\">";
            string userImage = getUserImage(user);
            string secondPart = "</div><div class=\"forumCommentText\"><div class=\"forumCommentInfo\">";
            string username = getUsername(user) + " Replied " + getDate(post);
            string thirdpart = "  </div><br /> ";
            string comments = getComments(post);
            string fourthpart = "</div><div class=\"forumCommentRight\">";
            string Vvotes = getVotes(votes);
            string fithpart = "</div></div><br /><br />";

            html = firstPart + userImage +
                   secondPart + username +
                   thirdpart + comments +
                   fourthpart + Vvotes +
                   fithpart;
            return html;
        }

        public string getUserImage(Models.User user)
        {
            if (user == null)
            {
                return "";
            }
            string img = "";
            switch (user.UserTypeID)
            {
                case 0:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
                case 1:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
                case 2:
                    img = "<img src=\"../Images/moderator.png\" alt=\"\">";
                    break;
                case 3:
                    img = "<img src=\"../Images/bronzeuser.png\" alt=\"\">";
                    break;
                case 4:
                    img = "<img src=\"../Images/silveruser.png\" alt=\"\">";
                    break;
                case 5:
                    img = "<img src=\"../Images/golduser.png\" alt=\"\">";
                    break;
                default:
                    img = "<img src=\"../Images/basicuser.png\" alt=\"\">";
                    break;
            }

            return img;
        }

        public string getDate(Models.ForumPost post)
        {
            if(post == null)
            {
                return "";
            }
            if(post.DateOfPost.Date == DateTime.Now.Date)
            {
                return "Today at " + post.DateOfPost.ToString("HH:mm");
            }
            else
            {
                return "On " + post.DateOfPost.ToString("dd MMM yyyy");
            }
        }
        public string getUsername(Models.User user)
        {
            if(user == null)
            {
                return "";
            }
            return user.Username;
        }
        public string getComments(Models.ForumPost post)
        {
            if(post == null)
            {
                return "";
            }
            return post.PostContent;
        }
        public string getVotes(int votes)
        {
            return "";
         //   return "votes " + votes.ToString();
        }
        public string deleteComment(Models.ForumPost post)
        {
            return "<button class=\"normalButton\" id=\"post" + post.PostID +"\"  onclick=\"SetCommentIDAndClick('"+ post.PostID +"')\">Delete Post</button>";
        }
    }
}