﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AmazinBargin
{
    public partial class RecoverAccount : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label1.Text = "If an account with that email exists then you will receive an email shortly with instructions to recover your account";
            var Fuser = new Function.User();
            var user = Fuser.getAllUsers().Where(x => x.Email == TextBox1.Text).FirstOrDefault();
            if(user == null)
            {
                return;
            }
            var FRecovery = new Function.PasswordRecovery();
            var recover = FRecovery.getPasswordRecoverys(user.UserID);
            if(recover == null)
            {
                recover = new Models.PasswordRecovery(user.UserID,FRecovery.getRandomKeyWithPrefix("recover"),DateTime.Now);
            }else
            {
                recover.RecoveryKey = FRecovery.getRandomKeyWithPrefix("recover");
                recover.RecoveryRequested = DateTime.Now;
            }
            FRecovery.savePasswordRecovery(recover);

            var mail = new MailService.SendEmail();
            mail.sendemailRecoveryUser(user, Request.Url.ToString().Substring(0, Request.Url.ToString().LastIndexOf('/')) + "/"+ "Recover.aspx?key=" + recover.RecoveryKey);
        }
    }
}