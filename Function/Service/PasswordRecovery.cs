namespace Service
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PasswordRecovery")]
    public partial class PasswordRecovery
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int UserID { get; set; }

        [Required]
        public string RecoveryKey { get; set; }

        public DateTime RecoveryRequested { get; set; }

        public virtual User User { get; set; }
    }
}
