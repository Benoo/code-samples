namespace Service
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserDealLike
    {
        public int UserDealLikeID { get; set; }

        public int UserID { get; set; }

        public int DealID { get; set; }

        public bool? LikedDeal { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual User User { get; set; }
    }
}
