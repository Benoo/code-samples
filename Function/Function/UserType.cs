﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class UserType
    {
        public void saveUserType(Models.UserType userType)
        {
            using(var service = new Service.AmazinBarginDB())
            {
                var r = service.UserTypes.Where(x => x.UserTypeID == userType.UserTypeID).FirstOrDefault();
                if(r == null)
                {
                    //Add New
                    r = new Service.UserType();
                    r.UserType1 = userType.UserType1;
                    r.UserTypeDescription = userType.UserTypeDescription;
                    service.UserTypes.Add(r);
                }else
                {
                    //Edit
                    r.UserType1 = userType.UserType1;
                    r.UserTypeDescription = userType.UserTypeDescription;
                }
                service.SaveChanges();
            }         
        }

        public Models.UserType getUserType(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserTypes.Where(x => x.UserTypeID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.UserType(r.UserTypeID, r.UserType1, r.UserTypeDescription);
                }      
            }
        }

        public List<Models.UserType> getAllUserTypes()
        {
            var userTypes = new List<Models.UserType>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserTypes.ToList();
                foreach(var usertype in r)
                {
                    userTypes.Add(new Models.UserType(usertype.UserTypeID, usertype.UserType1, usertype.UserTypeDescription));
                }              
            }
            return userTypes;
        }

        public void deleteUserType(Models.UserType userType)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserTypes.Where(x => x.UserTypeID == userType.UserTypeID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.UserTypes.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
