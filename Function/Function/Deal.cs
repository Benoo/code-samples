﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Deal
    {
        public void saveDeal(Models.Deal Deal)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Deals.Where(x => x.DealID == Deal.DealID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.Deal();
                    r.CodeWord = Deal.CodeWord;
                    r.DateEnd = Deal.DateEnd;
                    r.DateStart = Deal.DateStart;
                    r.DealPrice = Deal.DealPrice;
                    r.OriginalPrice = Deal.OriginalPrice;
                    r.SavingAmount = Deal.SavingAmount;
                    r.DealImage = Deal.DealImage;
                    service.Deals.Add(r);
                }
                else
                {
                    //Edit
                    r.CodeWord = Deal.CodeWord;
                    r.DateEnd = Deal.DateEnd;
                    r.DateStart = Deal.DateStart;
                    r.DealPrice = Deal.DealPrice;
                    r.OriginalPrice = Deal.OriginalPrice;
                    r.SavingAmount = Deal.SavingAmount;
                    r.DealImage = Deal.DealImage;
                }
                service.SaveChanges();
                Deal.DealID = r.DealID;
            }
        }

        public Models.Deal getDeals(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Deals.Where(x => x.DealID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.Deal(r.DealID, r.SavingAmount, r.OriginalPrice, r.CodeWord, r.DealPrice, r.DateStart, r.DateEnd, r.DealImage);
                }
            }
        }

        public List<Models.Deal> getAllDeals()
        {
            var Deals = new List<Models.Deal>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Deals.ToList();
                foreach (var Deal in r)
                {
                    Deals.Add(new Models.Deal(Deal.DealID, Deal.SavingAmount, Deal.OriginalPrice, Deal.CodeWord, Deal.DealPrice, Deal.DateStart, Deal.DateEnd, Deal.DealImage));
                }
            }
            return Deals;
        }

        public void deleteDeal(Models.Deal Deal)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Deals.Where(x => x.DealID == Deal.DealID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.Deals.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
