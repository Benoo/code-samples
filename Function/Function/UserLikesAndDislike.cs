﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class UserLikesAndDislike
    {
        public void saveUserLikesAndDislike(Models.UserLikesAndDislike UserLikesAndDislike)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserLikesDislikes.Where(x => x.UserLikesDislikeID == UserLikesAndDislike.UserLikesDislikeID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.UserLikesDislike();
                    r.LikedPost = UserLikesAndDislike.LikedPost;
                    r.PostID = UserLikesAndDislike.PostID;
                    r.UserID = UserLikesAndDislike.UserID;

                    service.UserLikesDislikes.Add(r);
                }
                else
                {
                    //Edit
                    r.LikedPost = UserLikesAndDislike.LikedPost;
                    r.PostID = UserLikesAndDislike.PostID;
                    r.UserID = UserLikesAndDislike.UserID;
                }
                service.SaveChanges();
            }
        }

        public Models.UserLikesAndDislike getUserLikesAndDislikes(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserLikesDislikes.Where(x => x.UserLikesDislikeID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.UserLikesAndDislike(r.UserLikesDislikeID,r.UserID,r.PostID,r.LikedPost);
                }
            }
        }

        public List<Models.UserLikesAndDislike> getAllUserLikesAndDislikes()
        {
            var UserLikesAndDislikes = new List<Models.UserLikesAndDislike>();
            using (var service = new Service.AmazinBarginDB())
            {
                var UserLikesAndDislike = service.UserLikesDislikes.ToList();
                foreach (var r in UserLikesAndDislike)
                {
                    UserLikesAndDislikes.Add(new Models.UserLikesAndDislike(r.UserLikesDislikeID, r.UserID, r.PostID, r.LikedPost));
                }
            }
            return UserLikesAndDislikes;
        }

        public void deleteUserLikesAndDislike(Models.UserLikesAndDislike UserLikesAndDislike)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserLikesDislikes.Where(x => x.UserLikesDislikeID == UserLikesAndDislike.UserLikesDislikeID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.UserLikesDislikes.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
