﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Subscription
    {
        public void saveSubscription(Models.Subscription Subscription)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Subscriptions.Where(x => x.SubscriptionID == Subscription.SubscriptionID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.Subscription();
                    r.ForumID = Subscription.ForumID;     
                    r.UserID = Subscription.UserID;

                    service.Subscriptions.Add(r);
                }
                else
                {
                    //Edit
                    r.ForumID = Subscription.ForumID;
                    r.UserID = Subscription.UserID;
                }
                service.SaveChanges();
            }
        }

        public Models.Subscription getSubscriptions(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Subscriptions.Where(x => x.SubscriptionID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.Subscription(r.SubscriptionID,r.UserID,r.ForumID);
                }
            }
        }

        public List<Models.Subscription> getAllSubscriptions()
        {
            var Subscriptions = new List<Models.Subscription>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Subscriptions.ToList();
                foreach (var Subscription in r)
                {
                    Subscriptions.Add(new Models.Subscription(Subscription.SubscriptionID, Subscription.UserID, Subscription.ForumID));
                }
            }
            return Subscriptions;
        }

        public void deleteSubscription(Models.Subscription Subscription)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Subscriptions.Where(x => x.SubscriptionID == Subscription.SubscriptionID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.Subscriptions.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
