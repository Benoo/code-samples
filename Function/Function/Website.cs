﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Website
    {
        public void saveWebsite(Models.Website Website)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Websites.Where(x => x.WebsiteID == Website.WebsiteID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.Website();
                    r.WebsiteLink = Website.WebsiteLink;
                    r.WebsiteName = Website.WebsiteName;
                    service.Websites.Add(r);
                }
                else
                {
                    //Edit
                    r.WebsiteLink = Website.WebsiteLink;
                    r.WebsiteName = Website.WebsiteName;
                }
                service.SaveChanges();
                Website.WebsiteID = r.WebsiteID;
            }
        }

        public Models.Website getWebsites(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Websites.Where(x => x.WebsiteID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.Website(r.WebsiteID,r.WebsiteName,r.WebsiteLink);
                }
            }
        }

        public List<Models.Website> getAllWebsites()
        {
            var Websites = new List<Models.Website>();
            using (var service = new Service.AmazinBarginDB())
            {
                var Website = service.Websites.ToList();
                foreach (var r in Website)
                {
                    Websites.Add(new Models.Website(r.WebsiteID, r.WebsiteName, r.WebsiteLink));
                }
            }
            return Websites;
        }

        public void deleteWebsite(Models.Website Website)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Websites.Where(x => x.WebsiteID == Website.WebsiteID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.Websites.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
