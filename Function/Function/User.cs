﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class User
    {
        public void saveUser(Models.User User)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Users.Where(x => x.UserID == User.UserID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.User();
                    r.BannedUntil = User.BannedUntil;
                    r.Email = User.Email;
                    r.Gender = User.Gender;
                    r.JoinDate = User.JoinDate;
                    r.LastVisitedDate = User.LastVisitedDate;
                    r.Location = User.Location;
                    r.Password = User.Password;
                    r.UserImageLink = User.UserImageLink;
                    r.Username = User.Username;
                    r.VerifiedEmail = User.VerifiedEmail;
                    r.WebsiteURL = User.WebsiteURL;
                    r.UserTypeID = User.UserTypeID;
                    r.UsersLastIP = User.UsersLastIP;
                    service.Users.Add(r);
                }
                else
                {
                    //Edit
                    r.BannedUntil = User.BannedUntil;
                    r.Email = User.Email;
                    r.Gender = User.Gender;
                    r.JoinDate = User.JoinDate;
                    r.LastVisitedDate = User.LastVisitedDate;
                    r.Location = User.Location;
                    r.Password = User.Password;
                    r.UserImageLink = User.UserImageLink;
                    r.Username = User.Username;
                    r.VerifiedEmail = User.VerifiedEmail;
                    r.WebsiteURL = User.WebsiteURL;
                    r.UserTypeID = User.UserTypeID;
                    r.UsersLastIP = User.UsersLastIP;         
                }
                service.SaveChanges();
                User.UserID = r.UserID;
            }
        }

        public Models.User getUsers(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Users.Where(x => x.UserID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.User(r.UserID,r.Username,r.Email,r.Password,r.UserImageLink,r.Gender,r.Location,r.WebsiteURL,r.JoinDate,r.BannedUntil,r.VerifiedEmail,r.UserTypeID,r.LastVisitedDate,r.UsersLastIP);
                }
            }
        }

        public List<Models.User> getAllUsers()
        {
            var Users = new List<Models.User>();
            using (var service = new Service.AmazinBarginDB())
            {
                var User = service.Users.ToList();
                foreach (var r in User)
                {
                    Users.Add(new Models.User(r.UserID, r.Username, r.Email, r.Password, r.UserImageLink, r.Gender, r.Location, r.WebsiteURL, r.JoinDate, r.BannedUntil, r.VerifiedEmail, r.UserTypeID, r.LastVisitedDate,r.UsersLastIP));
                }
            }
            return Users;
        }

        public void deleteUser(Models.User User)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Users.Where(x => x.UserID == User.UserID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.Users.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
