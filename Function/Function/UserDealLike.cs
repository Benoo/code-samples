﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class UserDealLike
    {
        public void saveUserDealLike(Models.UserDealLike UserDealLike)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserDealLikes.Where(x => x.UserDealLikeID == UserDealLike.UserDealLikeID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.UserDealLike();
                    r.DealID = UserDealLike.DealID;
                    r.LikedDeal = UserDealLike.LikedDeal;
                    r.UserID = UserDealLike.UserID;

                    service.UserDealLikes.Add(r);
                }
                else
                {
                    //Edit
                    r.DealID = UserDealLike.DealID;
                    r.LikedDeal = UserDealLike.LikedDeal;
                    r.UserID = UserDealLike.UserID;
                }
                service.SaveChanges();
            }
        }

        public Models.UserDealLike getUserDealLikes(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserDealLikes.Where(x => x.UserDealLikeID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.UserDealLike(r.UserDealLikeID, r.UserID, r.DealID, r.LikedDeal);
                }
            }
        }

        public List<Models.UserDealLike> getAllUserDealLikes()
        {
            var UserDealLikes = new List<Models.UserDealLike>();
            using (var service = new Service.AmazinBarginDB())
            {
                var UserDealLike = service.UserDealLikes.ToList();
                foreach (var r in UserDealLike)
                {
                    UserDealLikes.Add(new Models.UserDealLike(r.UserDealLikeID, r.UserID, r.DealID, r.LikedDeal));
                }
            }
            return UserDealLikes;
        }

        public void deleteUserDealLike(Models.UserDealLike UserDealLike)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.UserDealLikes.Where(x => x.UserDealLikeID == UserDealLike.UserDealLikeID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.UserDealLikes.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
