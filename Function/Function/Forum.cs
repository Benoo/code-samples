﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Forum
    {
        public void saveForum(Models.Forum Forum)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Forums.Where(x => x.ForumID == Forum.ForumID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.Forum();
                    r.CategoryID = Forum.CategoryID;
                    r.DateOfFounding = Forum.DateOfFounding;
                    r.DealID = Forum.DealID;
                    r.Description = Forum.Description;
                    r.ForumName = Forum.ForumName;
                    r.FoundingUserID = Forum.FoundingUserID;
                    r.Views = Forum.Views;
                    r.WebsiteID = Forum.WebsiteID;
                    r.WebsiteLink = Forum.WebsiteLink;
                    
                    service.Forums.Add(r);
                }
                else
                {
                    //Edit
                    r.CategoryID = Forum.CategoryID;
                    r.DateOfFounding = Forum.DateOfFounding;
                    r.DealID = Forum.DealID;
                    r.Description = Forum.Description;
                    r.ForumName = Forum.ForumName;
                    r.FoundingUserID = Forum.FoundingUserID;
                    r.Views = Forum.Views;
                    r.WebsiteID = Forum.WebsiteID;
                    r.WebsiteLink = Forum.WebsiteLink;
                }
                service.SaveChanges();
                Forum.ForumID = r.ForumID;
            }
        }

        public Models.Forum getForums(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Forums.Where(x => x.ForumID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.Forum(r.ForumID,r.ForumName,r.CategoryID,r.FoundingUserID,r.Description,r.DateOfFounding,r.WebsiteID,r.WebsiteLink,r.Views,r.DealID);
                }
            }
        }

        public List<Models.Forum> getAllForums()
        {
            var Forums = new List<Models.Forum>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Forums.ToList();
                foreach (var Forum in r)
                {
                    Forums.Add(new Models.Forum(Forum.ForumID, Forum.ForumName, Forum.CategoryID, Forum.FoundingUserID, Forum.Description, Forum.DateOfFounding, Forum.WebsiteID, Forum.WebsiteLink, Forum.Views, Forum.DealID));
                }
            }
            return Forums;
        }

        public void deleteForum(Models.Forum Forum)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Forums.Where(x => x.ForumID == Forum.ForumID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                var s = r.Subscriptions.ToList();             
                service.Subscriptions.RemoveRange(s);
                var deal = r.Deal;
                var deallikes = deal.UserDealLikes.ToList();
                service.UserDealLikes.RemoveRange(deallikes);
                service.Deals.Remove(deal);
                var posts = r.ForumsPosts.ToList();
                foreach(var post in posts)
                {
                    var postLikes = post.UserLikesDislikes.ToList();
                    service.UserLikesDislikes.RemoveRange(postLikes);
                    service.ForumsPosts.Remove(post);
                }

                service.Forums.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
