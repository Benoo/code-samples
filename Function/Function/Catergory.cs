﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class Catergory
    {
        public void saveCatergory(Models.Catergory Catergory)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Catergories.Where(x => x.CatergoryID == Catergory.CatergoryID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.Catergory();
                    r.CategoryDescription = Catergory.CategoryDescription;
                    r.CatergoryName = Catergory.CatergoryName;
                    r.DealOrForum = Catergory.DealOrForum;
                    service.Catergories.Add(r);
                }
                else
                {
                    //Edit
                    r.CategoryDescription = Catergory.CategoryDescription;
                    r.CatergoryName = Catergory.CatergoryName;
                }
                service.SaveChanges();
            }
        }

        public Models.Catergory getCatergory(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Catergories.Where(x => x.CatergoryID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.Catergory(r.CatergoryID, r.CatergoryName, r.CategoryDescription,r.DealOrForum);
                }
            }
        }

        public List<Models.Catergory> getAllCatergorys()
        {
            var Catergorys = new List<Models.Catergory>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Catergories.ToList();
                foreach (var Catergory in r)
                {
                    Catergorys.Add(new Models.Catergory(Catergory.CatergoryID, Catergory.CatergoryName, Catergory.CategoryDescription,Catergory.DealOrForum));
                }
            }
            return Catergorys;
        }

        public void deleteCatergory(Models.Catergory Catergory)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.Catergories.Where(x => x.CatergoryID == Catergory.CatergoryID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.Catergories.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
