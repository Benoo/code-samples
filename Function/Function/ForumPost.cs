﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class ForumPost
    {
        public void saveForumPost(Models.ForumPost ForumPost)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.ForumsPosts.Where(x => x.PostID == ForumPost.PostID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.ForumsPost();
                    r.DateOfPost = ForumPost.DateOfPost;
                    r.ForumID = ForumPost.ForumID;
                    r.PostContent = ForumPost.PostContent;
                    r.ReplyToPostID = ForumPost.ReplyToPostID;
                    r.UserID = ForumPost.UserID;
                  
                    service.ForumsPosts.Add(r);
                }
                else
                {
                    //Edit                 
                    r.DateOfPost = ForumPost.DateOfPost;
                    r.ForumID = ForumPost.ForumID;
                    r.PostContent = ForumPost.PostContent;
                    r.ReplyToPostID = ForumPost.ReplyToPostID;
                    r.UserID = ForumPost.UserID;
                }
                service.SaveChanges();
            }
        }

        public Models.ForumPost getForumPosts(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.ForumsPosts.Where(x => x.PostID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.ForumPost(r.PostID, r.ForumID, r.UserID, r.DateOfPost, r.PostContent, r.ReplyToPostID);
                }
            }
        }

        public List<Models.ForumPost> getAllForumPosts()
        {
            var ForumPosts = new List<Models.ForumPost>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.ForumsPosts.ToList();
                foreach (var ForumPost in r)
                {
                    ForumPosts.Add(new Models.ForumPost(ForumPost.PostID, ForumPost.ForumID, ForumPost.UserID, ForumPost.DateOfPost, ForumPost.PostContent, ForumPost.ReplyToPostID));
                }
            }
            return ForumPosts;
        }

        public void deleteForumPost(Models.ForumPost ForumPost)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.ForumsPosts.Where(x => x.PostID == ForumPost.PostID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                //Save Remove post find all likes
                var likes = r.UserLikesDislikes.ToList();
                service.UserLikesDislikes.RemoveRange(likes);
                //Okay safe to delete

                service.ForumsPosts.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
