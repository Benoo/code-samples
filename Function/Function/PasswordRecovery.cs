﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Function
{
    public class PasswordRecovery
    {
        public string getRandomKeyWithPrefix(string prefix)
        {
            int n = 30;
            string key = prefix;
            string ran = "";
            var random = new Random((int)DateTime.Now.Ticks + DateTime.Now.Second);
            string pic = "abcdefghijklmnopqrstuvwyxzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789875285328925723109341";
            for (var i = 0; i < n; i++)
            {
                ran += pic.Substring(random.Next(0, 70), 1);
            }
            return key + ran;
        }

        public void savePasswordRecovery(Models.PasswordRecovery PasswordRecovery)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.PasswordRecoveries.Where(x => x.UserID == PasswordRecovery.UserID).FirstOrDefault();
                if (r == null)
                {
                    //Add New
                    r = new Service.PasswordRecovery();
                    r.RecoveryKey = PasswordRecovery.RecoveryKey;
                    r.UserID = PasswordRecovery.UserID;
                    r.RecoveryRequested = PasswordRecovery.RecoveryRequested;

                    service.PasswordRecoveries.Add(r);
                }
                else
                {
                    //Edit
                    r.RecoveryKey = PasswordRecovery.RecoveryKey;
                    //r.UserID = PasswordRecovery.UserID;
                    r.RecoveryRequested = PasswordRecovery.RecoveryRequested;
                }
                service.SaveChanges();
            }
        }

        public Models.PasswordRecovery getPasswordRecoverys(int id)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.PasswordRecoveries.Where(x => x.UserID == id).FirstOrDefault();
                if (r == null)
                {
                    return null;
                }
                else
                {
                    return new Models.PasswordRecovery(r.UserID, r.RecoveryKey, r.RecoveryRequested);
                }
            }
        }

        public List<Models.PasswordRecovery> getAllPasswordRecoverys()
        {
            var PasswordRecoverys = new List<Models.PasswordRecovery>();
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.PasswordRecoveries.ToList();
                foreach (var PasswordRecovery in r)
                {
                    PasswordRecoverys.Add(new Models.PasswordRecovery(PasswordRecovery.UserID, PasswordRecovery.RecoveryKey, PasswordRecovery.RecoveryRequested));
                }
            }
            return PasswordRecoverys;
        }

        public void deletePasswordRecovery(Models.PasswordRecovery PasswordRecovery)
        {
            using (var service = new Service.AmazinBarginDB())
            {
                var r = service.PasswordRecoveries.Where(x => x.UserID == PasswordRecovery.UserID).FirstOrDefault();
                if (r == null)
                {
                    return;
                }
                service.PasswordRecoveries.Remove(r);
                service.SaveChanges();
            }
        }
    }
}
