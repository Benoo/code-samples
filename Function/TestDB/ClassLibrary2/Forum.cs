namespace ClassLibrary2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Forums")]
    public partial class Forum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Forum()
        {
            ForumsPosts = new HashSet<ForumsPost>();
            Subscriptions = new HashSet<Subscription>();
        }

        public int ForumID { get; set; }

        public string ForumName { get; set; }

        public int CategoryID { get; set; }

        public int FoundingUserID { get; set; }

        public string Description { get; set; }

        public DateTime DateOfFounding { get; set; }

        public int? WebsiteID { get; set; }

        public string WebsiteLink { get; set; }

        public int Views { get; set; }

        public int? DealID { get; set; }

        public virtual Catergory Catergory { get; set; }

        public virtual Deal Deal { get; set; }

        public virtual User User { get; set; }

        public virtual Website Website { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumsPost> ForumsPosts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Subscription> Subscriptions { get; set; }
    }
}
