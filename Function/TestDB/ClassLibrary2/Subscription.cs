namespace ClassLibrary2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Subscription
    {
        public int SubscriptionID { get; set; }

        public int UserID { get; set; }

        public int? ForumID { get; set; }

        public virtual Forum Forum { get; set; }

        public virtual User User { get; set; }
    }
}
