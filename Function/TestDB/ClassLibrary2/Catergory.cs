namespace ClassLibrary2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Catergory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Catergory()
        {
            Forums = new HashSet<Forum>();
        }

        public int CatergoryID { get; set; }

        [StringLength(50)]
        public string CatergoryName { get; set; }

        [StringLength(50)]
        public string CategoryDescription { get; set; }

        public bool? DealOrForum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Forum> Forums { get; set; }
    }
}
