namespace ClassLibrary2
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserLikesDislike
    {
        public int UserLikesDislikeID { get; set; }

        public int UserID { get; set; }

        public int PostID { get; set; }

        public bool? LikedPost { get; set; }

        public virtual ForumsPost ForumsPost { get; set; }

        public virtual User User { get; set; }
    }
}
