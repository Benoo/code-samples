﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace MailService
{
    public class SendEmail
    {
        public string serviceemail = "";
        public string testemaailto = "";
        public string smtp = "";

        public void sendemail(string emailAddress, string content, string subject)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.To.Add(emailAddress); 
            mailMessage.From = new MailAddress(serviceemail);
            mailMessage.Subject = subject;
            mailMessage.Body = content;
            SmtpClient smtpClient = new SmtpClient("");
          //  smtpClient.Send(mailMessage);
        }

        public void sendemailRecoveryUser(Models.User user,string link = "")
        {
            string emailAddres = user.Email;
            string content = @"To [username]

                                We have received a request for password recovery relating to this email address. If this was you please click the following link within the next 12 hours. When you click the link you will be directed to the Amazin Bargins website where you will be able to enter your username and your new password.

                                [link]

                                We look forward to you being able to access our service shortly.";
            content = content.Replace("[link]", link);
            content = content.Replace("[username]", user.Username);       
            sendemail(emailAddres, content, "Amazin Bargins --- Account Recovery");
        }

        public void sendemailVerifyUser(Models.User user, string link = "")
        {
            string emailAddres = user.Email;
            string content = @"Thank you for signing up to Amazin Bargins. To be able to create posts we need to verify your email address. Please click the link below or enter it into your browser to complete the sign up process.

                                [link]

                                If you haven't already you can sign up to our newsletter which lists the top bargains and any deals or forums you have subscribed to.
                               ";
            content = content.Replace("[link]", link);
            sendemail(emailAddres, content, "Amazin Bargins --- Account Creation");
        }
    }
}
