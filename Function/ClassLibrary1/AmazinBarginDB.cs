namespace ClassLibrary1
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AmazinBarginDB : DbContext
    {
        public AmazinBarginDB()
            : base("name=AmazinBarginDB")
        {
        }

        public virtual DbSet<Catergory> Catergories { get; set; }
        public virtual DbSet<Deal> Deals { get; set; }
        public virtual DbSet<Forum> Forums { get; set; }
        public virtual DbSet<ForumsPost> ForumsPosts { get; set; }
        public virtual DbSet<Subscription> Subscriptions { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<UserDealLike> UserDealLikes { get; set; }
        public virtual DbSet<UserLikesDislike> UserLikesDislikes { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserType> UserTypes { get; set; }
        public virtual DbSet<Website> Websites { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Catergory>()
                .HasMany(e => e.Forums)
                .WithRequired(e => e.Catergory)
                .HasForeignKey(e => e.CategoryID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deal>()
                .Property(e => e.SavingAmount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Deal>()
                .Property(e => e.OriginalPrice)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Deal>()
                .Property(e => e.DealPrice)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Deal>()
                .HasMany(e => e.UserDealLikes)
                .WithRequired(e => e.Deal)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Forum>()
                .HasMany(e => e.ForumsPosts)
                .WithRequired(e => e.Forum)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ForumsPost>()
                .HasMany(e => e.ForumsPosts1)
                .WithOptional(e => e.ForumsPost1)
                .HasForeignKey(e => e.ReplyToPostID);

            modelBuilder.Entity<ForumsPost>()
                .HasMany(e => e.UserLikesDislikes)
                .WithRequired(e => e.ForumsPost)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Forums)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.FoundingUserID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ForumsPosts)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Subscriptions)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserDealLikes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserLikesDislikes)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<UserType>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.UserType)
                .WillCascadeOnDelete(false);
        }
    }
}
