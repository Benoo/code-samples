namespace ClassLibrary1
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class ForumsPost
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ForumsPost()
        {
            ForumsPosts1 = new HashSet<ForumsPost>();
            UserLikesDislikes = new HashSet<UserLikesDislike>();
        }

        [Key]
        public int PostID { get; set; }

        public int ForumID { get; set; }

        public int UserID { get; set; }

        public DateTime DateOfPost { get; set; }

        public string PostContent { get; set; }

        public int? ReplyToPostID { get; set; }

        public virtual Forum Forum { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ForumsPost> ForumsPosts1 { get; set; }

        public virtual ForumsPost ForumsPost1 { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserLikesDislike> UserLikesDislikes { get; set; }
    }
}
