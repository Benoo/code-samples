﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserLikesAndDislike
    {
        public UserLikesAndDislike(int InUserLikesDislikeID, int InUserID, int InPostID, bool? InLikedPost)
        {
            UserLikesDislikeID = InUserLikesDislikeID;
            UserID = InUserID;
            PostID = InPostID;
            LikedPost = InLikedPost;
        }

        public int UserLikesDislikeID { get; set; }
        public int UserID { get; set; }
        public int PostID { get; set; }
        public bool? LikedPost { get; set; }
    }
}
