﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class PasswordRecovery
    {
        public PasswordRecovery(int InUserID, string InRecoveryKey, DateTime InRecoveryRequested)
        {
            UserID = InUserID;
            RecoveryKey = InRecoveryKey;
            RecoveryRequested = InRecoveryRequested;
        }
        public int UserID { get; set; }
        public string RecoveryKey { get; set; }
        public DateTime RecoveryRequested { get; set; }     

    }
    //This is used to both verify and recover an account
    //Verify account has pretext 'verify'
    //Recover account has pretext 'recover'
}
