﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Catergory
    {
        public Catergory(int InCatergoryID, string InCatergoryName, string InCategoryDescription, bool? InDealOrForum)
        {
            CatergoryID = InCatergoryID;
            CatergoryName = InCatergoryName;
            CategoryDescription = InCategoryDescription;
            DealOrForum = InDealOrForum;
        }

        public int CatergoryID { get; set; }      
        public string CatergoryName { get; set; }        
        public string CategoryDescription { get; set; }
        public bool? DealOrForum { get; set; }
    }
}
