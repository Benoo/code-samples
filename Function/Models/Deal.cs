﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Deal
    {
        public Deal(int InDealID, decimal? InSavingAmount, decimal? InOriginalPrice, string InCodeWord, decimal? InDealPrice, DateTime InDateStart, DateTime InDateEnd, string InDealImage)
        {
            DealID = InDealID;
            SavingAmount = InSavingAmount;
            OriginalPrice = InOriginalPrice;
            CodeWord = InCodeWord;
            DealPrice = InDealPrice;
            DateStart = InDateStart;
            DateEnd = InDateEnd;
            DealImage = InDealImage;
        }

        public int DealID { get; set; }

        public decimal? SavingAmount { get; set; }

        public decimal? OriginalPrice { get; set; }

        public string CodeWord { get; set; }

        public decimal? DealPrice { get; set; }

        public DateTime DateStart { get; set; }

        public DateTime DateEnd { get; set; }

        public string DealImage { get; set; }
    }
}
