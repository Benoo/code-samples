﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Models
{
    public class Website
    {
        public Website(int InWebsiteID, string InWebsiteName, string InWebsiteLink)
        {
            WebsiteID = InWebsiteID;
            WebsiteName = InWebsiteName;
            WebsiteLink = InWebsiteLink;
        }

        public int WebsiteID { get; set; }
        public string WebsiteName { get; set; }
        public string WebsiteLink { get; set; }

    }
}
