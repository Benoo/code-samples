﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class User
    {
        public User(int InUserID, string InUsername, string InEmail, string InPassword, string InUserImageLink, string InGender, string InLocation, string InWebsiteURL, DateTime InJoinDate, DateTime? InBannedUntil, bool InVerifiedEmail, int InUserTypeID, DateTime InLastVisitedDate, string InUsersLastIP)
        {
            UserID = InUserID;
            Username = InUsername;
            Email = InEmail;
            Password = InPassword;
            UserImageLink = InUserImageLink;
            Gender = InGender;
            Location = InLocation;
            WebsiteURL = InWebsiteURL;
            JoinDate = InJoinDate;
            BannedUntil = InBannedUntil;
            VerifiedEmail = InVerifiedEmail;
            UserTypeID = InUserTypeID;
            LastVisitedDate = InLastVisitedDate;
            UsersLastIP = InUsersLastIP;
        }

        public int UserID { get; set; }    
        public string Username { get; set; }     
        public string Email { get; set; }       
        public string Password { get; set; }
        public string UserImageLink { get; set; }    
        public string Gender { get; set; }     
        public string Location { get; set; }       
        public string WebsiteURL { get; set; }
        public DateTime JoinDate { get; set; }
        public DateTime? BannedUntil { get; set; }
        public bool VerifiedEmail { get; set; }
        public int UserTypeID { get; set; }
        public DateTime LastVisitedDate { get; set; }
        public string UsersLastIP { get; set; }
    }
}
