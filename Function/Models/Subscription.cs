﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Subscription
    {
        public Subscription(int InSubscriptionID, int InUserID, int? InForumID)
        {
            SubscriptionID = InSubscriptionID;
            UserID = InUserID;
            ForumID = InForumID;
        }

        public int SubscriptionID { get; set; }
        public int UserID { get; set; }
        public int? ForumID { get; set; }
    }
}
