﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserType
    {
        public UserType(int InUserTypeID, string InUserType1, string InUserTypeDescription)
        {
            UserTypeID = InUserTypeID;
            UserType1 = InUserType1;
            UserTypeDescription = InUserTypeDescription;
        }

        public int UserTypeID { get; set; }
        public string UserType1 { get; set; }
        public string UserTypeDescription { get; set; }
    }
}
