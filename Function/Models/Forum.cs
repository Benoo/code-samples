﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class Forum
    {
        public Forum(int InForumID, string InForumName, int InCategoryId, int InFoundingUserID, string InDescription, DateTime InDateOfFounding, int? InWebsiteID, string InWebsiteLink, int InViews, int? InDealID)
        {
            ForumID = InForumID;
            ForumName = InForumName;
            CategoryID = InCategoryId;
            FoundingUserID = InFoundingUserID;
            Description = InDescription;
            DateOfFounding = InDateOfFounding;
            WebsiteID = InWebsiteID;
            WebsiteLink = InWebsiteLink;
            Views = InViews;
            DealID = InDealID;
        }

        public int ForumID { get; set; }
        public string ForumName { get; set; }
        public int CategoryID { get; set; }
        public int FoundingUserID { get; set; }
        public string Description { get; set; }
        public DateTime DateOfFounding { get; set; }
        public int? WebsiteID { get; set; }
        public string WebsiteLink { get; set; }
        public int Views { get; set; }
        public int? DealID { get; set; }
    }
}
