﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class ForumPost
    {
        public ForumPost(int InPostID, int InForumID, int InUserID, DateTime InDateOfPost, string InPostContent, int? InReplyToPostID)
        {
            PostID = InPostID;
            ForumID = InForumID;
            UserID = InUserID;
            DateOfPost = InDateOfPost;
            PostContent = InPostContent;
            ReplyToPostID = InReplyToPostID;
        }

        public int PostID { get; set; }
        public int ForumID { get; set; }
        public int UserID { get; set; }
        public DateTime DateOfPost { get; set; }
        public string PostContent { get; set; }
        public int? ReplyToPostID { get; set; }
    }
}
