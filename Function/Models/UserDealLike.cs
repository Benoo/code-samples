﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class UserDealLike
    {
        public UserDealLike(int InUserDealLikeID, int InUserID, int InDealID, bool? InLikedDeal)
        {
            UserDealLikeID = InUserDealLikeID;
            UserID = InUserID;
            DealID = InDealID;
            LikedDeal = InLikedDeal;
        }

        public int UserDealLikeID { get; set; }
        public int UserID { get; set; }
        public int DealID { get; set; }
        public bool? LikedDeal { get; set; }
    }
}
