﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class WorkStatistics
    {
        public WorkStatistics()
        {
            JobID = new List<int>();
            JobTitle = new List<string>();
            JobDescription = new List<string>();
            Years = new List<int>();
            YearStart = new List<string>();
            YearEnd = new List<string>();
        }

        public List<int> JobID { get; set; }
        public List<string> JobTitle { get; set; }
        public List<string> JobDescription { get; set; }
        public List<int> Years { get; set; }
        public List<string> YearStart { get; set; }
        public List<string> YearEnd { get; set; }  

        public WorkStatistics getDefaultData()
        {
            JobID.Add(0);
            JobTitle.Add("Retail - Ferrari Formalwear");
            JobDescription.Add("<b>Retail - Ferrari Formalwear</b></br>Worked as a shoe shiner.");
            Years.Add(1);
            YearStart.Add("2011");
            YearEnd.Add("2011");

            JobID.Add(1);
            JobTitle.Add("Internship - SK Games");
            JobDescription.Add("<b>Internship - SK Games</b></br> A short stint as a Game Programmer at a game development studio in Perth WA. Introduced to the game development environment and professional game developers.</br></br>"
                             + "Skills Used: Unity");
            Years.Add(1);
            YearStart.Add("2012");
            YearEnd.Add("2012");


            JobID.Add(2);
            JobTitle.Add("Retail Assistant - Big W");
            JobDescription.Add("<b>Retail Assistant - Big W</b></br> Store presentation and helping customers.");
            Years.Add(4);
            YearStart.Add("2012");
            YearEnd.Add("2012");


            JobID.Add(3);
            JobTitle.Add("Start Up - Lets Go Events");
            JobDescription.Add("<b>Start Up - Lets Go Events</b></br> Co-Founded an app startup. App failed to launch successfully. I created the app and the backend. The other co-founder did the social media work.</br></br>"
                             + "Skills Used: Android Studio, Java, PHP, Javascript, Linux");
            Years.Add(1);
            YearStart.Add("2012");
            YearEnd.Add("2012");

            JobID.Add(4);
            JobTitle.Add("Lead Software Engineer - Red Ochre Software");
            JobDescription.Add("<b>Lead Software Engineer - Red Ochre Software</b></br> Main programmer for all company projects. Worked on projects for very large clients with 1000’s of employees."
                        + "Inherited bug ridden software and significantly improved the performance as well as created and released software from the ground up working alongside the project manager.</br></br>"
                        + "Skills Used: C#, Java, Android Studio, Javascript, ASP.net, MVC, HTML, CSS, Servers, MySQL, MSSQL, IOS");
            Years.Add(3);
            YearStart.Add("2012");
            YearEnd.Add("2012");

            return this;
        }
    }
}