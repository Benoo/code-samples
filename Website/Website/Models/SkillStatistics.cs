﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class SkillStatistics
    {
        public SkillStatistics()
        {
            id = new List<int>();
            skillQty = new List<int>();
            skillName = new List<string>();
        }

        public List<int> id { get; set; }
        public List<int> skillQty { get; set; }
        public List<string> skillName { get; set; }

        public SkillStatistics getDefaultData()
        {
            id.Add(0);
            skillQty.Add(10);
            skillName.Add("C#");

            id.Add(1);
            skillQty.Add(10);
            skillName.Add("Java");

            id.Add(2);
            skillQty.Add(10);
            skillName.Add("Javascript");

            id.Add(3);
            skillQty.Add(10);
            skillName.Add("PHP");

            id.Add(4);
            skillQty.Add(10);
            skillName.Add("Android");

            id.Add(5);
            skillQty.Add(10);
            skillName.Add("SQL");

            id.Add(6);
            skillQty.Add(10);
            skillName.Add("API");

            return this;
        }
    }
}