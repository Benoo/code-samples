﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Website.Models
{
    public class ProjectStatistics
    {
        public ProjectStatistics()
        {
            Projects = new List<ProjectStatistic>();
        }
        public List<ProjectStatistic> Projects { get; set; }

        public ProjectStatistics getDefaultData()
        {

            var projectStatistic = new ProjectStatistic
            {
                ProjectID = 0,
                ProjectQty = 4,
                ProjectType = "Mining Software",
                ProjectImage = "../../Images/mining.png"
            };

            Projects.Add(projectStatistic);

            var projectStatistic1 = new ProjectStatistic
            {
                ProjectID = 1,
                ProjectQty = 10,
                ProjectType = "Personal Projects",
                ProjectImage = "../../Images/project.png"
            };

            Projects.Add(projectStatistic1);

            var projectStatistic2 = new ProjectStatistic
            {
                ProjectID = 2,
                ProjectQty = 3,
                ProjectType = "Startups",
                ProjectImage = "../../Images/startup.png"
            };

            Projects.Add(projectStatistic2);

            var projectStatistic3 = new ProjectStatistic
            {
                ProjectID = 3,
                ProjectQty = 2,
                ProjectType = "Management Software",
                ProjectImage = "../../Images/management.png"
            };

            Projects.Add(projectStatistic3);

            return this;
        }
    }
    public class ProjectStatistic
    {
        public int ProjectID { get; set; }
        public int ProjectQty { get; set; }
        public string ProjectType { get; set; }
        public string ProjectImage { get; set; }
    }
}