﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;
using Website.Models;

namespace Website.Controllers
{
    public class WorkStatsController : ApiController
    {
        public string hello()
        {
            return "Hello World";
        }

        public string getWorkStatistics()
        {
            var workstats = new WorkStatistics();
            workstats.getDefaultData();
            return new JavaScriptSerializer().Serialize(workstats);
        }

        public string getProjectStatistics()
        {
            var projectStats = new ProjectStatistics();
            projectStats.getDefaultData();
            return new JavaScriptSerializer().Serialize(projectStats);
        }

        public string getSkillStatistics()
        {
            var skillStats = new SkillStatistics();
            skillStats.getDefaultData();
            return new JavaScriptSerializer().Serialize(skillStats);
        }

        [HttpPost]
        public string postWorkStatistics([FromBody] string info)
        {
            var workstats = new WorkStatistics();
            workstats.getDefaultData();
            if(workstats.JobTitle.Contains(info))
            {
                var index = workstats.JobTitle.IndexOf(info);
                return workstats.JobDescription[index];
            }
            return "";
        }

        [HttpPost]
        public string postProjectStatistics([FromBody] string info)
        {
            var workstats = new ProjectStatistics();
            workstats.getDefaultData();
            var project = workstats.Projects.Where(x => x.ProjectType == info).FirstOrDefault();
            if (project != null)
            {
                return project.ProjectImage;
            }
            return "";
        }
    }
}
