# Code Samples

Coding Examples

There are 3 sample projects in this repo. Details as follows:

File Named: Website
Description: This is a 3D website written using phoria.js
Directions: Use the arrow keys to move around and click on rocks for more infomation

File Named: MySite
Description: This is a dashboard website that gets its data using a web api. The website uses test data. 
Directions: After the dashboard is created click on items to learn more about them.

File Named: Function
Description: This is a database driven c# forum website.
Directions: Unpack AmazinBargins.bacpac to SQL Server the bacpac contains sample data. Connect the website to the database with the config file.
You can configure the email smtp and email providers in the MailService SendEmail.cs file or use the details of a user from the sample data.


